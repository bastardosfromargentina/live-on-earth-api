# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on "turbolinks:load", ->
  comment_form = $("#new_comment")

  $(comment_form).on "keypress", (e) ->
    if e && e.keyCode == 13
      e.preventDefault()
      $(@).submit()
  comment_form.on "submit", (e) ->
    e.preventDefault()

    comment = 
      article:
        id: comment_form.data("article-id")
      body: comment_form.find("#comment_body").val()
    comment_form.find("#comment_body").val("")
    App.comments.send_comment comment