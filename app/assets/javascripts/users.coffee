# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

bindUserRelationActionsBtns = (e) ->
  e.preventDefault()
  fs = #_action = #frienship_action
    "sent": 1
    "accept": 2
    "cancel_got": 3
    "delete": 4
    "text": 5
    "cancel_sent": 6
  userId = $(@).closest("[data-behaviour='users__relation__actions']").data("user-id")
  actionId = Number $(@).data("action-id")

  switch actionId
    when fs.sent
      App.friendship.create {id: userId}
    when fs.accept
      App.friendship.update {id: userId}
    when fs.cancel_got
      App.friendship.delete {id: userId}
    when fs.delete
      App.friendship.delete {id: userId}
    when fs.text
      $("#"+$(@).data("open")).foundation("open")
      App.chatrooms.create {id: userId}
    when fs.cancel_sent
      App.friendship.delete {id: userId}

  return false

$(document).on "turbolinks:load", ->
  usersRelationActions$ = $("[data-behaviour='users__relation__actions']")
  $(usersRelationActions$).find("a").on "click", bindUserRelationActionsBtns

  owlOptions =
    items: 1
    loop: true
    dots: false

  $(".user-profile-gallery").owlCarousel(owlOptions)

$(document).on "turbolinks:before-cache", () ->
  $(".user-profile-gallery").owlCarousel('destroy');
