# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

lithuaniaCenter = 
  lat: 55.147614
  lng: 24.4555564

initThisMap = (mapDom, place) ->
  bounds = new google.maps.LatLngBounds()
  map = new google.maps.Map mapDom, {}

  location = 
    lat: place.latitude
    lng: place.longitude
  marker = new google.maps.Marker
    map: map
    title: place.name
    position: location

  bounds.extend marker.position       
  map.fitBounds bounds

  listener = google.maps.event.addListener map, "idle", () ->
    map.setZoom(8); 
    google.maps.event.removeListener(listener)
    
initMap = (mapDom, placesData) ->
  bounds = new google.maps.LatLngBounds()
  map = new google.maps.Map mapDom,
    center: 
      lat: 55.147614
      lng: 24.4555564
    zoom: 8
  markers = []
  infoWindows = []
  markerIndex = 0
  for place in placesData
    location = 
      lat: place.latitude
      lng: place.longitude

    marker = new google.maps.Marker
      map: map
      title: place.name
      position: location

    marker.place_full_description = ""
    if place.name isnt ""
      marker.place_full_description += "<h4>#{place.name}</h4><br>"
    if place.description isnt ""
      marker.place_full_description += "<p>#{place.description}</p>"

    marker.infoWindow = new google.maps.InfoWindow
      content: marker.place_full_description

    markers.push marker
    bounds.extend marker.position 
    google.maps.event.addListener marker, 'click', () ->
      closeAllGMInfoWindows markers
      @.infoWindow.open map, @
         
  map.fitBounds bounds
  listener = google.maps.event.addListener map, "idle", () ->
    map.setZoom(8); 
    google.maps.event.removeListener(listener)
  
closeAllGMInfoWindows = (markers) ->
  for marker in markers
    marker.infoWindow.close()

$(document).on "turbolinks:load", ->
  # Place#show
  thisPlaceMapDom = document.getElementById('place-map')  
  if thisPlaceMapDom?
    $.ajax window.location.pathname,
      type: 'GET'
      dataType: 'json'
      error: (jqXHR, textStatus, errorThrown) ->
        console.log "AJAX Error: #{textStatus}"
      success: (data, textStatus, jqXHR) ->
        initThisMap thisPlaceMapDom, data

  owlOptions =
    items: 1
    loop: false
    dots: false
    nav: false

  $(".places-photos").owlCarousel(owlOptions)

  # Place#index
  mapDom = document.getElementById('map')  
  if mapDom?
    $.ajax '/places.json' + window.location.search,
      type: 'GET'
      dataType: 'json'
      error: (jqXHR, textStatus, errorThrown) ->
        console.log "AJAX Error: #{textStatus}"
      success: (data, textStatus, jqXHR) ->
        initMap mapDom, data

  map = undefined
  markers = []
  initPlaceMap = (mapDom) ->
    map = new google.maps.Map mapDom,
      center: lithuaniaCenter
      zoom: 8

    input = document.getElementById "pac-input"
    searchBox = new google.maps.places.SearchBox input
    options =
      componentRestrictions:
        country: "lt"
    autocomplete = new google.maps.places.Autocomplete(input, options);

    addMarker()

    map.addListener "bounds_changed", -> 
      searchBox.setBounds map.getBounds()

    map.addListener "click", (e) ->
      addMarker e.latLng

    searchBox.addListener "places_changed", ->
      places = searchBox.getPlaces()

      if places.length != 1
        return

      clearMarkers()
      
      bounds = new google.maps.LatLngBounds()
      place = places[0]

      if !place.geometry 
        console.log "Returned place contains no geometry" 
        return

      markers.push new google.maps.Marker(
        map: map
        title: place.name
        position: place.geometry.location
      )

      setFormData place
 
      bounds.extend place.geometry.location        
      map.fitBounds bounds

  setFormData = (place) ->
    placeForm$ = $("[data-behaviour='place']")
    placeForm$.find("#place_name").val(place.name) 
    placeForm$.find("#place_longitude").val(place.geometry.location.lng())
    placeForm$.find("#place_latitude").val(place.geometry.location.lat())

  addMarker = (location) ->
    clearMarkers()
    if location?
      marker = new google.maps.Marker
        position: location
        map: map
      marker.name = ""
    else
      placeForm$ = $("[data-behaviour='place']")
      latLng = 
        lat: parseFloat placeForm$.find("#place_latitude").val()
        lng: parseFloat placeForm$.find("#place_longitude").val()
      marker = new google.maps.Marker
        position: latLng
        map: map
      marker.name = placeForm$.find("#place_name").val()
    
    markers.push(marker)
    
    marker.geometry =
      location: marker.position
    setFormData marker

  setMapOnOne = (map, marker) ->
      marker.setMap map

  setMapOnAll = (map) ->
    for marker in markers
      marker.setMap map

  clearMarkers = ->
    setMapOnAll(null);

  deleteMarkers = ->
    clearMarkers()
    markers = []

  placeMapDom = document.getElementById("place_map")
  if placeMapDom?
    initPlaceMap placeMapDom

$(document).on "turbolinks:before-cache", () ->
  $(".places-photos").owlCarousel('destroy');