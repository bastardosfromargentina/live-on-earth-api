# Global variables
directionsService = undefined
directionsRenderer = undefined
routePlanListSortable = undefined
updatePlacesVal = ->
  $("#places").val routePlanListSortable.toArray()
  calculateAndDisplayRoute directionsService, directionsRenderer

computeTotalDistance = (result) ->
  totalDist = 0
  totalTime = 0
  myroute = result.routes[0]

  for route_leg in myroute.legs
    totalDist += route_leg.distance.value
    totalTime += route_leg.duration.value
  
  totalDist = (totalDist / 1000).toFixed(2)
  totalTime = (totalTime / 60).toFixed(2)
  $(".duration").val(totalTime).html(totalTime)
  $(".distance").val(totalDist).html(totalDist)

displayTripRoute = (directionsService, directionsRenderer, data) -> # show
  wayPoints = []
  $.each data.places, (index, el) ->
    wayPoints.push
      location: 
        lat: el.location.latitude
        lng: el.location.longitude
      stopover: true
  if wayPoints.length >= 2
    origingLoc = wayPoints.shift()
    destinationLoc = wayPoints.pop()
    directionsService.route
      origin: origingLoc.location
      destination: destinationLoc.location
      waypoints: wayPoints
      optimizeWaypoints: false
      travelMode: 'DRIVING'
    , (response, status) ->
      if status is 'OK'
        directionsRenderer.setDirections response 
        route = response.routes[0]
      else
        window.alert('Directions request failed due to ' + status)

calculateAndDisplayRoute = (directionsService, directionsRenderer) ->
  wayPoints = []
  $.each $("ul#route-plan-list").find(".place__info"), (index, el) ->
    wayPoints.push
      location: {lat: $(el).data("place-position-lat"), lng: $(el).data("place-position-lng")}
      stopover: true
  console.log wayPoints
  if wayPoints.length >= 2
    origingLoc = wayPoints.shift()
    destinationLoc = wayPoints.pop()
    directionsService.route
      origin: origingLoc.location #wayPoints[0].position
      destination: destinationLoc.location  #wayPoints[wayPoints.length - 1].position
      waypoints: wayPoints
      optimizeWaypoints: false
      travelMode: 'DRIVING'
    , (response, status) ->
      if status is 'OK'
        directionsRenderer.setDirections response 
        route = response.routes[0]
        computeTotalDistance(response)
      else
        window.alert('Directions request failed due to ' + status)

  
initNewTripRouteMap = (newTripRouteMap) ->
  newTripRouteMap = new google.maps.Map $(newTripRouteMap).get(0),
    zoom: 6
    center: 
      lat: 55.147614
      lng: 24.4555564
  directionsRenderer.setMap newTripRouteMap
  calculateAndDisplayRoute directionsService, directionsRenderer

movePlaceToTripPlaces = (placeObj, routePlanningTable$) ->
  routePlaceHtml = 
    """
    <li class="list-group-item place__info route-plan-item"
        data-place-name="#{placeObj.name}"
        data-place-id="#{placeObj.id}"
        data-place-type-id="#{placeObj.place_type_id}"
        data-place-position-lat="#{placeObj.position.lat}"
        data-place-position-lng="#{placeObj.position.lng}">
      <div class="row">
        <div class="small-2 columns plan-item-handler">
          <span><i class="fi-paw medium"></i></span>
        </div>
        <div class="small-8 columns place-name">
          #{placeObj.name}
        </div>
        <div class="small-2 columns plan-item-remove">
          <button data-behaviour="remove-selected-place"><i class="fi-x small"></i></button>
        </div>
      </div>
    </li>
    """
  $(routePlanningTable$)
    .append routePlaceHtml

  $(routePlanningTable$)
    .find "[data-place-id='#{placeObj.id}'] [data-behaviour='remove-selected-place']"
    .on "click", handleRemoveTripPlaceBtn

moveTripPlaceToPlaces = (place, placesTypeSelect$) ->
  placeActionsHtml = 
  """
  <div class="small-6 columns place-info"
    data-place-name="#{place.name}"
    data-place-id="#{place.id}"
    data-place-type-id="#{place.place_type_id}"
    data-place-position-lat="#{place.position.lat}"
    data-place-position-lng="#{place.position.lng}">
    <div class="card">
      <div class="card-section">
        <div class="row columns">
          #{place.name.split(' ')[0]}
        </div>
        <div class="row">
          <div class="columns">
            <button data-behaviour="add__place">
              <i class="fi-check"></i>
            </button>  
          </div>
          <div class="columns">
            <button>
              <i class="fi-x"></i>
            </button>  
          </div>
          <div class="columns">
            <button>
              <i class="fi-list"></i>
            </button>  
          </div>
        </div>
      </div>
    </div>
  </div>
  """
  placesTypeSelect$.prepend placeActionsHtml
  placesTypeSelect$
    .find "[data-place-id='#{place.id}'] [data-behaviour='add__place']"
    .on "click", handleAddTripPlaceBtn

handleAddTripPlaceBtn = (e) ->
  routePlanList$ = $("ul#route-plan-list")
  placeInfo$ = $(@).closest(".place-info")
  placeObj = 
    id: $(placeInfo$).data("place-id")
    name: $(placeInfo$).data("place-name")
    place_type_id: $(placeInfo$).data("place-type-id")
    position:
      lat: $(placeInfo$).data("place-position-lat")
      lng: $(placeInfo$).data("place-position-lng")

  movePlaceToTripPlaces placeObj, routePlanList$
  placeInfo$.remove()
  updatePlacesVal()

handleRemoveTripPlaceBtn = (e) ->
  placeTypes$ = $(".place-type")
  placeInfo$ = $(@).closest ".route-plan-item"
  placeObj = 
    id: $(placeInfo$).data("place-id")
    name: $(placeInfo$).data("place-name")
    place_type_id: Number($(placeInfo$).data("place-type-id"))
    position:
      lat: $(placeInfo$).data("place-position-lat")
      lng: $(placeInfo$).data("place-position-lng")

  placeType$ = undefined
  $.grep placeTypes$, (el) ->
    if $(el).data("place-type-id") is placeObj.place_type_id
      placeType$ = el
  if placeType$?
    moveTripPlaceToPlaces placeObj, $(placeType$).find ".place-type-content .place-type-places"
    $(placeInfo$).remove()
    updatePlacesVal()

handleVisitPlaceBtn = (e) ->
  e.preventDefault()
  place =
    id: $(@).data("place-id")
    visited: $(@).data("place-visited")
    selected_route_id: $(@).data("selected-route-id")
  App.trip_route.update_trip_route_place(place)
  $(@).closest(".reveal").foundation("close") #("##{$(@).data('open')}").foundation("close")

$(document).on "turbolinks:load", ->
  routePlanList$ = $("ul#route-plan-list")
  placeTypes$ = $(".place-type")

  placeTypes$
    .find("[data-behaviour='add__place']")
    .on "click", handleAddTripPlaceBtn

  routePlanList$
    .find "[data-behaviour='remove-selected-place']"
    .on "click", handleRemoveTripPlaceBtn

  if $(routePlanList$).length > 0
    routePlanListSortable = Sortable.create document.getElementById($(routePlanList$).attr("id")),
      dataIdAttr: "data-place-id"
      handle: ".fi-paw"
      animation: 150
      onUpdate: (evt) ->
        updatePlacesVal()

  newTripRouteMap = $("#new_trip_route_map")
  if newTripRouteMap.length > 0
    directionsService = new google.maps.DirectionsService
    directionsRenderer = new google.maps.DirectionsRenderer
    initNewTripRouteMap newTripRouteMap

  form$ = $("form[data-behaviour='trip-form']")        

  form$.find("#submit__button").on "click", (event) ->
    updatePlacesVal()
    form$.submit()
  $("[data-behaviour='update-trip-places']").on "click", handleVisitPlaceBtn
  window.handleVisitPlaceBtn = handleVisitPlaceBtn

  showTripRouteMapDom = $("#show_trip_route_map")
  if showTripRouteMapDom.length > 0
    $.ajax window.location.pathname,
      type: 'GET'
      dataType: 'json'
      error: (jqXHR, textStatus, errorThrown) ->
        console.log "AJAX Error: #{textStatus}"
      success: (data, textStatus, jqXHR) ->
        directionsService = new google.maps.DirectionsService
        directionsRenderer = new google.maps.DirectionsRenderer
        showTripRouteMap = new google.maps.Map $(showTripRouteMapDom).get(0),
        zoom: 6
        center: 
          lat: 55.147614
          lng: 24.4555564
        directionsRenderer.setMap showTripRouteMap
        console.dir data
        displayTripRoute directionsService, directionsRenderer, data

  $('.trip-route-main-photos').owlCarousel
    animateOut: 'fadeOut'
    animateIn: 'fadeIn'
    items: 1
    loop: true
    margin: 10
    autoplay: true
    autoplayTimeout: 1000
    onInitialized: (event) ->
      $('.trip-route-main-photos').trigger 'play.owl.autoplay',[1000]

  $('.trip-route-gallery').owlCarousel
    items: 1
    loop: true
    margin: 10
    dots: false
    responsiveClass:true
    responsive:
      0:
        items:1
        