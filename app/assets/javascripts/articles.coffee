$(document).on "turbolinks:load", ->

  String.prototype.empty = ->
    @.split(" ").join("").length == 0

  article_reveal = $("#new_article_reveal")
  article_form = $(article_reveal).find("form")

  $("#new_article_reveal").on "closed.zf.reveal", (e) ->
    article = {}
    article.id = $("#article_photo_article_id").val()
    if article.id? and article.id > 0
      App.articles.get_article article

  $("#new_article_submit_btn").on "click", (e) ->
    e.preventDefault()
    title = article_reveal.find("#article_title")
    body = article_reveal.find("#article_body")
    unless title.val().empty() or body.val().empty()
      $(@).attr("disabled", "")
      $(article_form).submit()

  $(article_form).on "submit", (e) ->
    e.preventDefault()
    title = article_reveal.find("#article_title")
    body = article_reveal.find("#article_body")
    trip_route_id = article_reveal.find("#trip_route_id")
    unless trip_route_id?
      trip_id = -1
    else
      trip_id = trip_route_id.val()

    article = 
      title: title.val()
      body: body.val()
      type: $("#article_type").val()
      trip_route_id: trip_id

    App.articles.send_article article
    title.val("")
    body.val("")      

  owlOptions =
    items: 1
    loop: true
    dots: false

  $(".article-photos").owlCarousel(owlOptions)

$(document).on "turbolinks:before-cache", () ->
  $(".article-photos").owlCarousel('destroy');
