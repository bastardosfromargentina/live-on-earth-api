App.comments = App.cable.subscriptions.create "CommentsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    console.log data
    $("#article-comments").prepend(data.htmlContent)

  send_comment: (comment) ->
    @perform "send_comment", comment
