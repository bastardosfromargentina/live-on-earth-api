App.articles = App.cable.subscriptions.create "ArticlesChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (article) ->
    a_status =
      INITIAL: 0
      CREATED: 1

    formHtml = ''
    article_form = undefined
    initDropzone = ->
      Dropzone.autoDiscover = false  
      dropzone = new Dropzone ".dropzone",
        maxFilesize: 256
        paramName: "article_photo[image]"
        addRemoveLinks: true
        headers: 
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        success: (file, response) ->
          $(file.previewTemplate).find('.dz-remove').attr('id', response.fileID)
          $(file.previewElement).addClass("dz-success")
        removedfile: (file) ->
          _this = this
          _file = file
          id = $(file.previewTemplate).find('.dz-remove').attr('id') 
          $.ajax
            type: 'DELETE',
            url: $(dropzones).attr("action") + "/" + id,
            success: (data) ->
              file.previewElement.remove()
              file=null

    article.status = Number(article.status)
    if a_status.INITIAL == article.status
      article_form = $("#new_article")
      formHtml = article_form.get(0).outerHTML
      article_form.replaceWith(article.addPhotosHtml)
      $("#new_article_photo").find("#article_photo_article_id").val(article.id)
      $("#new_article_photo").addClass("dropzone")
      if $(".dropzone").length > 0
        initDropzone()
    else if a_status.CREATED == article.status
      console.dir article
      article_form = $("#new_article_reveal").find("form")        
      articlesList = $("[data-behaviour='articles-list']")
      articlesList.prepend $(article.htmlContent)
      articlesList.find('.article-photos').first().owlCarousel
        items: 1
        loop: true
        dots: false
        center: false
      article_form.replaceWith(article.addPhotosHtml)

  send_article: (article) ->
    @perform "send_article", article

  get_article: (article) ->
    @perform "get_article", article