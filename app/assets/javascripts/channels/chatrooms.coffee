App.chatrooms = App.cable.subscriptions.create "ChatroomsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    console.log data
    active_chatroom = $("#chatroom-reveal#{data.chatroom_id}")
    if active_chatroom.length > 0

      if document.hidden
        if $(".strMessagike").length == 0
          active_chatroom.append("<div class='strike'><span>Unread Messages</span></div>")

        # if Notification.permission == "granted"
        #   new Notification(data.email, {body: data.body})

      else
        App.last_read.update(data.chatroom_id)

      # Insert the message

      active_chatroom.find("ul").append(data.messageHtml)
      active_chatroom.find("ul").scrolledDown()

    else
      $("[data-behavior='chatroom-link'][data-chatroom-id='#{data.chatroom_id}']").css("font-weight", "bold")

  create: (data) ->    
    @perform "create", {user: data}
    
  send_message: (chatroom_id, message) ->
    data = {chatroom_id: chatroom_id, body: message}
    @perform "send_message", data
