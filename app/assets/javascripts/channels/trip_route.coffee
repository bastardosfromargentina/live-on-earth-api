App.trip_route = App.cable.subscriptions.create "TripRouteChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    console.log data
    if data.action is "update_trip_route_place"
      $('[data-behaviour="trip-route-places"]').find("table")
        .find("[data-open='place-modal#{data.id}']")
        .parent()
        .html(data.htmlContent)

      $("#place-modal#{data.id}").html(data.modalHtml)
      if data.completed
        $("#place-modal#{data.id}").foundation('open')
        
      $("#place-modal#{data.id}")
        .find("[data-behaviour='update-trip-places'][data-place-id='#{data.id}']")
        .on "click", window.handleVisitPlaceBtn

  update_trip_route_place: (place) ->
    @perform "update_trip_places", place
