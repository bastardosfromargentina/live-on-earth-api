App.friendship = App.cable.subscriptions.create "FriendshipChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    bindUserRelationActionsBtns = (e) ->
      e.preventDefault()
      fs = #_action = #frienship_action
        "sent": 1
        "accept": 2
        "cancel_got": 3
        "delete": 4
        "text": 5
        "cancel_sent": 6

      userId = $(@).closest("[data-behaviour='users__relation__actions']").data("user-id")
      actionId = Number $(@).data("action-id")

      switch actionId
        when fs.sent
          App.friendship.create {id: userId}
        when fs.accept
          App.friendship.update {id: userId}
        when fs.cancel_got
          App.friendship.delete {id: userId}
        when fs.delete
          App.friendship.delete {id: userId}
        when fs.text
          modal$ = $("#"+$(@).data("open"))
          reveal = new Foundation.Reveal modal$
          modal$.foundation("open").initChatroomTab()
          App.chatrooms.create {id: userId}
        when fs.cancel_sent
          App.friendship.delete {id: userId}
      return false
    
    $("[data-behaviour='users__relation__actions'][data-user-id='#{data.user.id}']")
      .find ".button-group"
      .html data.actionHtml
      .find "a"
      .on "click", bindUserRelationActionsBtns
          
  create: (data) ->
    @perform "create", {user: data}

  update: (data) ->
    @perform "update", {user: data}

  delete: (data) ->
    @perform "delete", {user: data}

