App.achievments = App.cable.subscriptions.create "AchievmentsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    alert data.achievment.description
    # Called when there's incoming data on the websocket for this channel
