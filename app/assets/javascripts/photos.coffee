# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http:

$(document).on "turbolinks:load", ->
  dropzones = $("#new_photo").addClass("dropzone")

  $(dropzones).dropzone
    url: $(@).attr("action")
    maxFilesize: 2
    paramName: $(@).find("input[type='file']").attr("name")
    addRemoveLinks: true
    success: (file, response) ->
      $(file.previewTemplate).find('.dz-remove').attr('id', response.fileID)
      $(file.previewElement).addClass("dz-success")
    removedfile: (file) ->
      _this = this
      _file = file
      id = $(file.previewTemplate).find('.dz-remove').attr('id') 
      $.ajax
        type: 'DELETE',
        url: $(dropzones).attr("action") + "/" + id,
        success: (data) ->
          file.previewElement.remove()
          file=null

  $("[data-behaviour='remove-photo']").on "click", (e) ->
    _this = this
    id = $(@).data('photo-id')
    $.ajax
      type: 'DELETE'
      url: $(dropzones).attr("action") + "/" + id
      success: (data) ->
        $(_this).closest("[data-behaviour='photo-wrapper']").remove()
