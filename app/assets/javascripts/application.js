//= require jquery
//= require jquery_ujs
//= require jquery.turbolinks
//= require owl-carousel2
//= require dropzone
//= require foundation-sites
//= require motion-ui
//= require Sortable
//= require turbolinks
//= require_tree

$(document).on("turbolinks:load", function() {
  $(document).foundation();
  
  var owlOptions = {
  items: 3,
  loop: true,
  dots: false,
  center: false,
  responsiveClass:true,
  responsive:{
      0:{
        items:1
      },
      480:{
        items:2
      },
      640:{
        items:3
      },
      1024:{
        items:4
      }
    }
  }; 
  
  $('#users__grid').owlCarousel(owlOptions);
  $('#places__grid').owlCarousel(owlOptions);
  $('.user-profile-gallery').owlCarousel(owlOptions);
  $("#trip-route-places-owl").owlCarousel(owlOptions);
});

$(document).on('turbolinks:before-cache', function() {
  $(".owl-carousel").owlCarousel('destroy');
});