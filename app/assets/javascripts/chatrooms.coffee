handleVisiblityChange = ->
  $strike = $(".strike")
  if $strike.length > 0
    chatroom_id = $("[data-behavior='messages']").data("chatroom-id")
    App.last_read.update(chatroom_id)
    $strike.remove()

$.fn.isolatedScroll = ->
  @on 'mousewheel DOMMouseScroll', (e) ->
    delta = e.wheelDelta or e.originalEvent and e.originalEvent.wheelDelta or -e.detail
    bottomOverflow = @scrollTop + $(@).outerHeight() - (@scrollHeight) >= 0
    topOverflow = @scrollTop <= 0
    if delta < 0 and bottomOverflow or delta > 0 and topOverflow then e.preventDefault()
$.fn.scrolledDown = ->
  @scrollTop($(@)[0].scrollHeight)
      
$.fn.initChatroomTab = ->
  $(@).find("ul")
  .isolatedScroll()
  .scrolledDown()

  chatForm = $(@).find("#new_message")
  $(chatForm).on "keypress", (e) ->
    if e && e.keyCode == 13
      e.preventDefault()
      $(@).submit()

  $(chatForm).on "submit", (e) ->
    e.preventDefault()

    chatroom_id = $(@).closest("[data-behavior='messages']").data("chatroom-id")
    body        = $(@).find("#message_body")
    console.log(chatroom_id + " - --" +  body.val())
    App.chatrooms.send_message(chatroom_id, body.val())
    body.val ""

$(document).on "turbolinks:load", ->
  $(document).on "click", handleVisiblityChange
  chatroomsContent$ = $(".chatroom-content")
  if chatroomsContent$.length > 0
    chatroomsContent$.initChatroomTab()
  chatroomReveal$ = $(".chatroom-reveal")
  if chatroomReveal$.length > 0
    chatroomReveal$.initChatroomTab()