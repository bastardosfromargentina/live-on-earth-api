json.extract! place, :id, :name, :description, :longitude, :latitude, :created_at, :updated_at
json.url place_url(place, format: :json)
