json.extract! trip_route, :id, :name, :user_id, :created_at, :updated_at
json.places do
  json.array!(trip_route.places_ordered) do |place|
    json.name place.name
    json.location do
      json.latitude place.latitude
      json.longitude place.longitude
    end
  end
end
json.url trip_route_url(trip_route, format: :json)