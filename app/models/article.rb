class Article < ApplicationRecord
  belongs_to :user
  has_many :article_comments, dependent: :destroy
  has_many :comments, through: :article_comments, dependent: :destroy
  has_many :article_photos, dependent: :destroy
  has_one :trip_route_article, dependent: :destroy
  has_one :trip_route, through: :trip_route_article

  enum topic: {general: 0, triproute: 1, place: 2}

  def photos
    article_photos
  end
end
