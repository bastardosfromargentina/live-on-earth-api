require "#{Rails.root}/lib/enums/friendship_action"
class Friendship < ApplicationRecord
  enum status: [ :sent, :got, :accepted ]
  
  belongs_to :user
  belongs_to :friend, class_name: 'User'

  after_update :create_chatroom

  before_destroy :destroy_relatives

  private

  def create_chatroom
    if accepted?
      @chatroom = get_chatroom
      unless @chatroom.present?
        @chatroom = Chatroom.create!(title: "#{@conversation_name}")
        @chatroom.users.push([user, friend])
        @chatroom.save!
      end
    end
  end

  def destroy_relatives
    chatroom = user.get_chatroom_with(friend)
    if chatroom.present?
      chatroom.destroy
    end
  end

  def get_chatroom
    @conversation_name = get_conversation_name
    Chatroom.
      where(title: ["#{@conversation_name.sort}",
          "#{@conversation_name.sort.reverse}"])
      .first
  end
  
  def get_conversation_name
    [user.id, friend.id]
  end

end
