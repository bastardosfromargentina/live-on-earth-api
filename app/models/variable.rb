class Variable < ApplicationRecord
  has_many :achievment_goals
  has_many :achievments, through: :achievment_goals
  has_many :user_variables
  has_many :users, through: :user_variables
end
