class Chatroom < ApplicationRecord
  has_many :chatroom_users, dependent: :delete_all 
  has_many :users, through: :chatroom_users
  has_many :messages, dependent: :delete_all

  def get_title(current_user)    
    chatroom_users = users.select do |user|
      not user.eql?(current_user)
    end
    chat_title = []
    chatroom_users.each do |user|
      chat_title.push(user.name)
    end
    chat_title.join(', ')
  end
end
