module Shared
  module AttachmentHelper

    def self.included(base)
      base.extend ClassMethods
    end

    module ClassMethods
      def has_attachment(name, options = {})

        # generates a string containing the singular model name and the pluralized attachment name.
        # Examples: "user_avatars" or "asset_uploads" or "message_previews"
        attachment_owner    = self.table_name.singularize
        attachment_folder   = "#{attachment_owner}_#{name.to_s.pluralize}"
       
        if Rails.env.production?
          options[:url] = ':live-on-earth.s3.amazonaws.com'
          options[:path] = '/:rails_env/:class/:attachment/:id_partition/:style/:filename'          

          options[:s3_host_name] = 's3-us-west-2.amazonaws.com'
          options[:s3_region] = 'us-west-2'
          options[:bucket] = 'live-on-earth'
          options[:storage]         ||= :s3
          options[:s3_credentials]  ||= Proc.new{|a| a.instance.s3_credentials }
          options[:s3_permissions]  ||= 'private'
          options[:s3_protocol]     ||= 'https'
        else
          # For local Dev/Test envs, use the default filesystem, but separate the environments
          # into different folders, so you can delete test files without breaking dev files.
          options[:path]  ||= ":rails_root/public/system/attachments/#{Rails.env}/#{attachment_path}"
          options[:url]   ||= "/system/attachments/#{Rails.env}/#{attachment_path}"
        end

        # pass things off to paperclip.
        has_attached_file name, options
      end

      def s3_credentials
        {
          bucket: "live-on-earth",
          access_key_id: ENV["AWS_ACCESS_KEY_ID"],
          secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"]
        }
      end
    end
  end
end