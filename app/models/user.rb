class User < ApplicationRecord
 paginates_per 8
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :chatroom_users, dependent: :destroy 
  has_many :chatrooms, through: :chatroom_users
  has_many :messages, dependent: :destroy
  has_many :friendships, dependent: :destroy
  has_many :places, dependent: :destroy
  has_many :articles, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :trip_routes
  has_many :user_selected_routes
  has_many :user_variables, dependent: :destroy
  has_many :variables, through: :user_variables
  has_many :user_achievments, dependent: :destroy
  has_many :achievments, through: :user_achievments
  has_many :user_photos, dependent: :destroy
  has_many :photos, through: :user_photos
  has_many :user_tags, dependent: :destroy
  has_many :tags, through: :user_tags
  
  after_create :set_default_photo
  def selected_routes
    user_selected_routes.collect {|route| route.trip_route }
  end

  def has_friend_request_from?(user)
    friendships.got.where(friend: user).exists?
  end

  def sent_friend_request_for?(user)
    friendships.sent.where(friend: user).exists?
  end

  def friend?(user)
    friendships.accepted.where(friend: user).exists?
  end

  def friends
    accepted_friends_ids = friendships.accepted.collect {|ff| ff.friend_id }
    User.find(accepted_friends_ids)
  end

  def other_users
    accepted_friends_ids = friendships.accepted.collect {|ff| ff.friend_id }.push(self.id)
    User.where.not(id: accepted_friends_ids)
  end

  def get_chatroom_with(user)
    chatroom = Chatroom
      .where(id: chatroom_users.pluck(:chatroom_id))
      .joins(:chatroom_users)
      .where(chatroom_users: {user_id: user.id})
      .last
    unless chatroom.present?
      chatroom = Chatroom.create!(title: "#{[self.id, user.id]}")
      chatroom.users.push([self, user])
      chatroom.save!
    end
    chatroom
  end

  def remove_default_photo
    photos.where(image_file_name: File.basename("#{Rails.root}/public/static_assets/default_user.png")).destroy_all
  end

  private

  def set_default_photo
    ph_file = File.open("#{Rails.root}/public/static_assets/default_user.png")
    photos.create!(image: ph_file)
    ph_file.close
  end

end