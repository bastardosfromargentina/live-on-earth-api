class UserSelectedRoute < ApplicationRecord
  belongs_to :user
  belongs_to :trip_route

  def trip_route_places    
    Place.find(places.keys).collect do |place|
      Place.new(
        id: place.id,
        name: place.name,
        visited: places[place.id.to_s].eql?('true')
      )
    end
  end

  def completed?
    places.key(false.to_s).nil?
  end

  after_update :add_achievment_points_to_user

  def add_achievment_points_to_user
    if completed?
      route_create_ach_var = Variable.find_by_name('completed_routes')
      user_variable = user.user_variables.where(variable_id: route_create_ach_var.id).first_or_create!
      user_variable.value += 1
      user_goals = AchievmentGoal.where(variable_id: route_create_ach_var.id, goal: user_variable.value)
      user_variable.save!
      if user_goals.present?
        user_goals.each do |user_goal|
          user_achievment = user.user_achievments.create!(achievment_id: user_goal.achievment.id)
          AchievmentRelayJob.perform_now(user_achievment)
        end
      end
    end
  end
end