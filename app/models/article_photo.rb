class ArticlePhoto < ApplicationRecord
  belongs_to :article

  has_attached_file :image,
    styles: {
      medium: "300x300#",
      thumb: "100x100#",
      panoram: "850x350#"
    }
  
  validates_attachment :image, 
    presence:  true,
    content_type:  { content_type: /\Aimage\/.*\Z/ }
end
