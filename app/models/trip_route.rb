class TripRoute < ApplicationRecord
  belongs_to :user
  has_many :trip_route_places, dependent: :destroy
  has_many :user_selected_routes, dependent: :destroy
  has_many :places, through: :trip_route_places
  has_many :trip_route_articles, dependent: :destroy
  has_many :articles, through: :trip_route_articles
  has_many :trip_route_photos, dependent: :destroy
  has_many :photos, through: :trip_route_photos
  
  scope :title_includes, -> (search_string) {
    where('lower(name) like ?', "%#{search_string}%".downcase)
  }

  scope :distance_length, -> (min_distance, max_distance) {
    where(distance: min_distance..max_distance)
  }

  scope :duration_length, -> (min_duration, max_duration) {
    where(duration: min_duration..max_duration)
  }

  scope :places_count, -> (min_count, max_count) {
    includes(:trip_route_places).all.select{|trip_route| trip_route.trip_route_places.count.in?((min_count.to_i..max_count.to_i).to_a)}
  }

  def places_to_visit
    place_ids.collect{ |p| [p, false] }.to_h
  end

  def places_ordered
    places.includes(:trip_route_places).order('trip_route_places.waypoint_nr')
  end

  def completed?
    places.where(visited: false).empty?
  end

  def photos
    places.select{|place| !place.photos.empty? }.collect do |place|
      !place.photos.empty? ? place.photos.first : next 
    end
  end
  
  private

  def add_achievment_points_to_user
    places_ach_variable = Variable.find_by_name('created_routes')
    user_variable = user.user_variables.where(variable: places_ach_variable).first_or_create
    user_variable.value += 1
    user_goals = AchievmentGoal.where(variable_id: places_ach_variable.id, goal: user_variable.value)
    user_variable.save!
    if user_goals.present?
      user_goals.each do |user_goal|
        user_achievment = user.user_achievments.create!(achievment: user_goal.achievment)
        AchievmentRelayJob.perform_now(user_achievment)
      end
    end
  end
end