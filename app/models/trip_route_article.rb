class TripRouteArticle < ApplicationRecord
  belongs_to :trip_route
  belongs_to :article
end
