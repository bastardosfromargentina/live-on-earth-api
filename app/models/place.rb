class Place < ApplicationRecord
  paginates_per 50
  belongs_to :user
  belongs_to :place_type
  has_many :trip_route_places, dependent: :destroy
  has_many :trip_routes, through: :trip_route_places
  has_many :place_tags
  has_many :tags, through: :place_tags
  has_many :place_photos, dependent: :destroy

  attr_accessor :visited

  scope :title_includes, -> (search_string) {
    where('lower(name) like ? or lower(description) like ?',
      "%#{search_string}%".downcase, "%#{search_string}%".downcase
      )
  }
  scope :type, -> (type_id) {
    where(place_type_id: type_id)
  }

  after_create :add_achievment_points_to_user, :set_default_photo

  def photos
    place_photos
  end

  def remove_default_photo
    photos.where(image_file_name: File.basename(default_image_path)).destroy_all
  end

  private

  def add_achievment_points_to_user
    places_ach_variable = Variable.find_by_name('created_places')
    user_variable = user.user_variables.where(variable: places_ach_variable).first_or_create
    user_variable.value += 1
    user_goals = AchievmentGoal.where(variable_id: places_ach_variable.id, goal: user_variable.value)
    user_variable.save!
    if user_goals.present?
      user_goals.each do |user_goal|
        user_achievment = user.user_achievments.create!(achievment: user_goal.achievment)
        AchievmentRelayJob.perform_now(user_achievment)
      end
    end
  end

  def default_image_path
    "#{Rails.root}/public/static_assets/default_place.png"
  end

  def set_default_photo
    ph_file = File.open(default_image_path)
    place_photos.create!(image: ph_file)
    ph_file.close
  end
end
