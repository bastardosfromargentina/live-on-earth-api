class PlaceType < ApplicationRecord
  has_many :places

  def places_not_associated_with_route(route)
    places.where.not(id: route.place_ids)   
  end
end
