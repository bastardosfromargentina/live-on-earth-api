class PlacePhotosController < ApplicationController
  before_action :set_place, only: [:index, :create]
  def index
  end

  def create
    @place.remove_default_photo
    @place_photo = @place.photos.create(place_photo_params)
    if @place_photo.save
      render json: { message: "success", fileID: @place_photo.id }, status: 200
    else
      render json: { error: @place_photo.errors.full_messages.join(',')}, status: 400
    end
  end

  def destroy
    @photo = PlacePhoto.find(params[:id])
    if @photo.destroy
      render json: { message: "File deleted from server" }
    else
      render json: { message: @photo.errors.full_messages.join(',') }
    end
  end

  private

  def set_place
    @place = Place.find(params[:place_id])
  end

  def place_photo_params
    params.require(:place_photo).permit(:image)
  end
end
