class UsersController < ApplicationController
  include UserChatrooms
  before_action :set_users, only: [:index]
  before_action :set_user, only: [:show]

  def index
    @trip_routes = TripRoute.first(10)
    @places = Place.first(8)
    @friends = current_user.friends
    @articles = Article.last(5)
  end

  def list_users
    @users = current_user.other_users.page(params[:page]).per(8)
  end

  def current_user_page
  end
  
  def show; end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def set_users
    @users = current_user.other_users.first(4) #.limit(5)
  end

end
