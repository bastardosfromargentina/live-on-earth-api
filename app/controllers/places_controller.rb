class PlacesController < ApplicationController
  before_action :set_place, only: [:show, :edit, :update, :destroy]
  before_action :set_place_types, only: [:new, :edit]
  has_scope :title_includes, :type
  # GET /places
  # GET /places.json
  def index
    @page_places = apply_scopes(Place).all.order(created_at: :desc).page(params[:page]).per(8)
    @places = apply_scopes(Place).all
  end

  def search
    @page_places = apply_scopes(Place).all.page(params[:page]).per(8)
    @places = apply_scopes(Place).all
  end
  # GET /places/1
  # GET /places/1.json
  def show
  end

  def user_places
    @places = current_user.places
  end
  # GET /places/new
  def new
    @place = Place.new
  end

  # GET /places/1/edit
  def edit
  end

  # POST /places
  # POST /places.json
  def create
    @place = Place.new(place_params)
    @place.user = current_user
    @place.place_type = PlaceType.find(params[:place][:place_type_id])
    respond_to do |format|
      if @place.save        
        format.html { redirect_to place_place_photos_path(@place), notice: 'Place was successfully created.' }
        # format.json { render :show, status: :created, location: @place }
      else
        format.html { render :new }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /places/1
  # PATCH/PUT /places/1.json
  def update
    respond_to do |format|
      if @place.update(place_params)
        format.html { redirect_to place_place_photos_path(@place), notice: 'Place was successfully created.' }
        format.json { render :show, status: :ok, location: @place }
      else
        format.html { render :edit }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy
    @place.destroy
    respond_to do |format|
      format.html { redirect_to places_url, notice: 'Place was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_place
      @place = Place.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def place_params
      params.require(:place).permit(:name, :description, :longitude, :latitude, :place_type_id)
    end

    def set_place_types
      @place_types = PlaceType.all
    end
end
