class PhotosController < ApplicationController

  def new
    @photo = Photo.new
    @photos = current_user.photos
  end
  
  def create    
    current_user.remove_default_photo
    @photo = current_user.photos.build(photo_params)
    if @photo.save
      render json: { message: "success", fileID: @photo.id }, :status => 200
    else
      render json: { error: @photo.errors.full_messages.join(',')}, :status => 400
    end     
  end

  def destroy
    @photo = Photo.find(params[:id])
    if @photo.destroy
      render json: { message: "File deleted from server" }
    else
      render json: { message: @photo.errors.full_messages.join(',') }
    end
  end

  private
  
  def photo_params
    params.require(:photo).permit(:image)
  end

end
