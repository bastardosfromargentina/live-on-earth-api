class ArticlePhotosController < ApplicationController
  
  def index
    @article_photos = ArticlePhoto.all
  end

  def create
    @article_photo = ArticlePhoto.new(article_photo_params)
    if @article_photo.save
      render json: { message: "success", fileID: @article_photo.id }, status: 200
    else
      render json: { error: @article_photo.errors.full_messages.join(',')}, status: 400
    end
  end

  def destroy
    @photo = ArticlePhoto.find(params[:id])
    if @photo.destroy
      render json: { message: "File deleted from server" }
    else
      render json: { message: @photo.errors.full_messages.join(',') }
    end
  end

  private

  def article_photo_params
    params.require(:article_photo).permit(:image, :article_id)
  end

end
