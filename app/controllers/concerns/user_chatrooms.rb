module UserChatrooms

  def get_chatroom
    Chatroom.
      where(title: ["#{@conversation_name.sort}",
          "#{@conversation_name.sort.reverse}"])
      .first
  end
  
  def set_conversation_name
    @conversation_name = [current_user.id, @user.id]
  end
  
  module_function :get_chatroom
end