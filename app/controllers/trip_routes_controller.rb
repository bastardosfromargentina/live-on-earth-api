class TripRoutesController < ApplicationController
  before_action :set_trip_route, only: [:show, :edit, :update, :destroy, :take_trip]
  before_action :trip_route_places, only: [:create, :update]
  before_action :set_place_types, only: [:new, :edit]

  has_scope :title_includes
  has_scope :distance_length, using: [:min_distance, :max_distance], type: :hash
  has_scope :duration_length, using: [:min_duration, :max_duration], type: :hash
  has_scope :places_count, using: [:min_count, :max_count], type: :hash

  # GET /trip_routes
  # GET /trip_routes.json
  def index
    @trip_routes = apply_scopes(TripRoute).all.order(created_at: :desc)
  end

  def search
    @trip_routes = apply_scopes(TripRoute) #.where("1=1")
  end

  def user_routes
    @user_selected_routes = current_user.selected_routes
    @trip_routes = current_user.trip_routes
  end
  # GET /trip_routes/1
  # GET /trip_routes/1.json
  def show
    @articles = @trip_route.articles
    @article = Article.new
    @trip_route_places = @trip_route.places_ordered.collect.with_index { |place, index|
      view_context.link_to("#{index + 1} #{place.name}", place_path(place))
    }.join(', ')
  end

  def take_trip
    @route = current_user.user_selected_routes
      .where(trip_route_id: @trip_route.id)
      .first_or_create!(
        places: @trip_route.places_to_visit
      )
    respond_to :html, :json
  end

  def update_trip_places;  end

  # GET /trip_routes/new
  def new
    @trip_route = TripRoute.new
  end

  # GET /trip_routes/1/edit
  def edit
  end

  # POST /trip_routes
  # POST /trip_routes.json
  def create
    @trip_route = TripRoute.new(trip_route_params)

    @trip_route.user = current_user
    respond_to do |format|
      if @trip_route.save
        @places.each_with_index do |place, order_nr|
          @trip_route.trip_route_places.create!(place: place, waypoint_nr: order_nr)
        end    

        format.html { redirect_to @trip_route, notice: 'Trip route was successfully created.' }
        format.json { render :show, status: :created, location: @trip_route }
      else
        format.html { render :new }
        format.json { render json: @trip_route.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trip_routes/1
  # PATCH/PUT /trip_routes/1.json
  def update
    # @trip_route.places = @places
    @trip_route.places.delete_all

    @places.each_with_index do |place, order_nr|
      @trip_route.trip_route_places.create!(place: place, waypoint_nr: order_nr)
    end

    respond_to do |format|
      if @trip_route.update(trip_route_params)
        format.html { redirect_to @trip_route, notice: 'Trip route was successfully updated.' }
        format.json { render :show, status: :ok, location: @trip_route }
      else
        format.html { render :edit }
        format.json { render json: @trip_route.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trip_routes/1
  # DELETE /trip_routes/1.json
  def destroy
    @trip_route.destroy
    respond_to do |format|
      format.html { redirect_to trip_routes_url, notice: 'Trip route was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_place_types
      @place_types = []
      pts = PlaceType.all
      pts.each { |place_type|
        @place_types.push(place_type) unless place_type.places.blank? 
      }
      @place_types
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_trip_route
      @trip_route = TripRoute.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trip_route_params
      params.require(:trip_route).permit(:name, :duration, :distance)
    end

    def trip_route_places
      @places_ids = params.require(:places).split(',').map{|place_id| place_id.to_i}
      @places = Place.find(@places_ids)
    end
end
