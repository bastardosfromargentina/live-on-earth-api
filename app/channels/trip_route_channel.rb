class TripRouteChannel < ApplicationCable::Channel
  
  def subscribed
    stream_from 'trip_routes'
  end

  def unsubscribed
    stop_all_streams
  end

  def update_trip_places(place_data)
    place_hash = get_place_data(place_data)
    route = current_user.user_selected_routes.find(place_data['selected_route_id'])
    route.places[@place.id.to_s] = @place.visited.to_s
    route.save!
    place_hash[:completed] = route.completed?
    TripRoutePlaceRelayJob.perform_later(place_hash)
  end

  private
  
  def get_place_data(place_data)                  
    @place = Place.find(place_data['id'])
    @place.visited = !place_data['visited']
    {
      place: @place,
      visited: !place_data['visited'],
      route_id: place_data['selected_route_id'].to_i
    }
  end

end
