require "#{Rails.root}/lib/enums/article_type"
require "#{Rails.root}/lib/enums/article_status"

class ArticlesChannel < ApplicationCable::Channel
  
  def subscribed
    stream_from "articles"
  end

  def unsubscribed
    stop_all_streams
  end

  def get_article(article)
    @article = Article.find(article["id"].to_i)
    if @article.present?
      ArticleRelayJob.perform_later(@article, ArticleStatus::CREATED)
    end
  end

  def send_article(article)
    @article = current_user.articles.build(
      title: article["title"],
      body: article["body"]
    )
    if @article.save!
      if article['type'].to_i == ArticleType::PLACE
        @article.general!
      elsif article['type'].to_i == ArticleType::TRIP_ROUTE and article['trip_route_id'].to_i > 0
        @trip_route = TripRoute.find(article['trip_route_id'])
        @article.triproute!
        @trip_route.articles.push(@article)
      else
        @article.place!
      end    
      ArticleRelayJob.perform_later(@article, ArticleStatus::INITIAL)
    end
  end

end