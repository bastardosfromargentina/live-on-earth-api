require "#{Rails.root}/lib/enums/friendship_action"

class FriendshipChannel < ApplicationCable::Channel

  def subscribed
    stream_from "friendship_#{current_user.id}"
  end

  def unsubscribed
    stop_all_streams
  end

  def create data
    user = User.find(data['user']['id']) 
    unique_fs_key = rand(36**8).to_s(36) 

    current_user.friendships.create!(friend: user, key_id: unique_fs_key).sent!
    user.friendships.create!(friend: current_user, key_id: unique_fs_key).got!

    FriendshipsRelayJob.perform_later(current_user, user, FriendshipAction::NEW)
  end

  def update data
    user = User.find(data['user']['id'])
    current_user.friendships.find_by_friend_id(user).accepted!
    user.friendships.find_by_friend_id(current_user).accepted!

    FriendshipsRelayJob.perform_later(current_user, user, FriendshipAction::ACCEPTED )
  end

  def delete data
    user = User.find(data['user']['id'])
    current_user.friendships.find_by_friend_id(user).destroy!
    user.friendships.find_by_friend_id(current_user).destroy!

    FriendshipsRelayJob.perform_later(current_user, user, FriendshipAction::DELETED )
  end
  
end

