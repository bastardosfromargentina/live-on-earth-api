class ChatroomsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "chatrooms"
  end

  def unsubscribed
    stop_all_streams 
  end

  def create(data)    
    @user = User.find(data['user']['id'])
    @chatroom = get_chatroom
    unless @chatroom
      @chatroom = Chatroom.create!(title: "#{@conversation_name}")
      @chatroom.users.push([current_user, @user])
      @chatroom.save!
    end
  end

  def send_message(data)
    @chatroom = Chatroom.find(data["chatroom_id"])
    message   = @chatroom.messages.create(body: data["body"], user: current_user)
    MessageRelayJob.perform_later(message)
  end

  private

  def get_chatroom
    @conversation_name = get_conversation_name
    Chatroom.
      where(title: ["#{@conversation_name.sort}",
          "#{@conversation_name.sort.reverse}"])
      .first
  end
  
  def get_conversation_name
    [current_user.id, @user.id]
  end

end
