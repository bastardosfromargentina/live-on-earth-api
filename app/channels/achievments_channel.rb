class AchievmentsChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'user_achievments'
  end

  def unsubscribed
    stop_all_streams
  end
end
