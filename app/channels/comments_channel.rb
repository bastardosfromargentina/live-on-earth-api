class CommentsChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'comments'
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
    stop_all_streams
  end

  def send_comment data
    article = Article.find(data['article']['id'])
    @comment = article.comments.create!(body: data['body'], user: current_user)
    CommentRelayJob.perform_later(@comment)
  end

end
