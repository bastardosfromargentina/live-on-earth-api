require "#{Rails.root}/lib/enums/friendship_action"

class FriendshipsRelayJob < ApplicationJob
  queue_as :default

  def perform(sender, recipient, friendship_action)

    friendship_action = FriendshipAction::SENT if friendship_action == FriendshipAction::NEW  

    ActionCable.server.broadcast(
      "friendship_#{sender.id}", {
        friendship_action: friendship_action,
        user: { id: recipient.id },
        actionHtml: render_friendship_action_content(sender, recipient)
      }
    )

    friendship_action = FriendshipAction::GOT if friendship_action == FriendshipAction::NEW  
    ActionCable.server.broadcast(
      "friendship_#{recipient.id}", {
        fs_action: friendship_action,
        user: { id: sender.id },
        actionHtml: render_friendship_action_content(recipient, sender)
      }
    )
  end

  private

  def render_friendship_action_content(sender, recipient)
    ApplicationController.renderer.render(
      partial: 'users/partials/friendship_actions',
      locals: {
        current_user: sender,
        user: recipient
      }
    )
  end

end
