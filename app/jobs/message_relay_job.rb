class MessageRelayJob < ApplicationJob
  queue_as :default

  def perform(message)
    ActionCable.server.broadcast "chatrooms", {
      messageHtml: get_message_html(message),
      chatroom_id: message.chatroom.id
    }
  end

  private

  def get_message_html(message)
    ApplicationController.renderer.render(
      partial: 'messages/message', locals: { message: message }
      )
  end
end
