class TripRoutePlaceRelayJob < ApplicationJob
  queue_as :default

  def perform(place_hash)
    html_content = ApplicationController.render(
      partial: 'trip_routes/partials/visited_button',
      locals: {
          place: place_hash[:place],
          visited: place_hash[:visited]
        }
      )
    modal_html = ApplicationController.render(
      partial: 'trip_routes/partials/selected_route_place_reveal',
      locals: {
          place: place_hash[:place],
          visited: place_hash[:visited],
          selected_route_id: place_hash[:route_id],
          completed: place_hash[:completed]
        }
      )
    ActionCable.server.broadcast 'trip_routes', {
      htmlContent: html_content,
      modalHtml: modal_html,
      action: 'update_trip_route_place', 
      id: place_hash[:place].id,
      completed: place_hash[:completed]
    }
  end
end
