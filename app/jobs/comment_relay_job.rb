class CommentRelayJob < ApplicationJob
  queue_as :default

  def perform(comment)
    ActionCable.server.broadcast 'comments', {
      htmlContent: ApplicationController
        .render(
          partial: 'comments/partials/comment',
          locals: { comment: comment }
        )
    }
  end
end
