class AchievmentRelayJob < ApplicationJob
  queue_as :default

  def perform(user_achievment)
    ActionCable.server.broadcast "user_achievments", {
      achievment: {
        description: user_achievment.achievment.description
      }
    }
  end
end
