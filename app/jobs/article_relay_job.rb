class ArticleRelayJob < ApplicationJob
  queue_as :default

  def perform(article, article_status)
    ActionCable.server.broadcast 'articles', {
      id: article.id,
      htmlContent: render_article(article),
      addPhotosHtml: render_photos_form(article, article_status),
      status: article_status
    }
  end

  private

  def render_article(article)
    ApplicationController.renderer.render(article)
  end

  def render_photos_form(article, article_status)
    htmlContent = ''
    if article_status == ArticleStatus::INITIAL
      htmlContent = ApplicationController.renderer.render(
        partial: 'article_photos/create'
      )
    elsif article_status == ArticleStatus::CREATED
      art_params = {}
      if article.trip_route.present? and article.triproute?
        art_params[:trip_route_id] = article.trip_route.id
      end
      htmlContent = ApplicationController.renderer.render(
        partial: 'articles/partials/form',
        locals: {
          article_type: article.topic_before_type_cast,
          param: art_params
        }
      )
    end
    htmlContent
  end

end
