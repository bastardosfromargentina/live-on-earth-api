(function() {
  var bindUserRelationActionsBtns;

  bindUserRelationActionsBtns = function(e) {
    var actionId, fs, userId;
    e.preventDefault();
    fs = {
      "sent": 1,
      "accept": 2,
      "cancel_got": 3,
      "delete": 4,
      "text": 5,
      "cancel_sent": 6
    };
    userId = $(this).closest("[data-behaviour='users__relation__actions']").data("user-id");
    actionId = Number($(this).data("action-id"));
    switch (actionId) {
      case fs.sent:
        App.friendship.create({
          id: userId
        });
        break;
      case fs.accept:
        App.friendship.update({
          id: userId
        });
        break;
      case fs.cancel_got:
        App.friendship["delete"]({
          id: userId
        });
        break;
      case fs["delete"]:
        App.friendship["delete"]({
          id: userId
        });
        break;
      case fs.text:
        $("#" + $(this).data("open")).foundation("open");
        App.chatrooms.create({
          id: userId
        });
        break;
      case fs.cancel_sent:
        App.friendship["delete"]({
          id: userId
        });
    }
    return false;
  };

  $(document).on("turbolinks:load", function() {
    var owlOptions, usersRelationActions$;
    usersRelationActions$ = $("[data-behaviour='users__relation__actions']");
    $(usersRelationActions$).find("a").on("click", bindUserRelationActionsBtns);
    owlOptions = {
      items: 1,
      loop: true,
      dots: false
    };
    return $(".user-profile-gallery").owlCarousel(owlOptions);
  });

  $(document).on("turbolinks:before-cache", function() {
    return $(".user-profile-gallery").owlCarousel('destroy');
  });

}).call(this);
