(function() {
  $(document).on("turbolinks:load", function() {
    var comment_form;
    comment_form = $("#new_comment");
    $(comment_form).on("keypress", function(e) {
      if (e && e.keyCode === 13) {
        e.preventDefault();
        return $(this).submit();
      }
    });
    return comment_form.on("submit", function(e) {
      var comment;
      e.preventDefault();
      comment = {
        article: {
          id: comment_form.data("article-id")
        },
        body: comment_form.find("#comment_body").val()
      };
      comment_form.find("#comment_body").val("");
      return App.comments.send_comment(comment);
    });
  });

}).call(this);
