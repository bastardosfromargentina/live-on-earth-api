(function() {
  var closeAllGMInfoWindows, initMap, initThisMap, lithuaniaCenter;

  lithuaniaCenter = {
    lat: 55.147614,
    lng: 24.4555564
  };

  initThisMap = function(mapDom, place) {
    var bounds, listener, location, map, marker;
    bounds = new google.maps.LatLngBounds();
    map = new google.maps.Map(mapDom, {});
    location = {
      lat: place.latitude,
      lng: place.longitude
    };
    marker = new google.maps.Marker({
      map: map,
      title: place.name,
      position: location
    });
    bounds.extend(marker.position);
    map.fitBounds(bounds);
    return listener = google.maps.event.addListener(map, "idle", function() {
      map.setZoom(8);
      return google.maps.event.removeListener(listener);
    });
  };

  initMap = function(mapDom, placesData) {
    var bounds, i, infoWindows, len, listener, location, map, marker, markerIndex, markers, place;
    bounds = new google.maps.LatLngBounds();
    map = new google.maps.Map(mapDom, {
      center: {
        lat: 55.147614,
        lng: 24.4555564
      },
      zoom: 8
    });
    markers = [];
    infoWindows = [];
    markerIndex = 0;
    for (i = 0, len = placesData.length; i < len; i++) {
      place = placesData[i];
      location = {
        lat: place.latitude,
        lng: place.longitude
      };
      marker = new google.maps.Marker({
        map: map,
        title: place.name,
        position: location
      });
      marker.place_full_description = "";
      if (place.name !== "") {
        marker.place_full_description += "<h4>" + place.name + "</h4><br>";
      }
      if (place.description !== "") {
        marker.place_full_description += "<p>" + place.description + "</p>";
      }
      marker.infoWindow = new google.maps.InfoWindow({
        content: marker.place_full_description
      });
      markers.push(marker);
      bounds.extend(marker.position);
      google.maps.event.addListener(marker, 'click', function() {
        closeAllGMInfoWindows(markers);
        return this.infoWindow.open(map, this);
      });
    }
    map.fitBounds(bounds);
    return listener = google.maps.event.addListener(map, "idle", function() {
      map.setZoom(8);
      return google.maps.event.removeListener(listener);
    });
  };

  closeAllGMInfoWindows = function(markers) {
    var i, len, marker, results;
    results = [];
    for (i = 0, len = markers.length; i < len; i++) {
      marker = markers[i];
      results.push(marker.infoWindow.close());
    }
    return results;
  };

  $(document).on("turbolinks:load", function() {
    var addMarker, clearMarkers, deleteMarkers, initPlaceMap, map, mapDom, markers, owlOptions, placeMapDom, setFormData, setMapOnAll, setMapOnOne, thisPlaceMapDom;
    thisPlaceMapDom = document.getElementById('place-map');
    if (thisPlaceMapDom != null) {
      $.ajax(window.location.pathname, {
        type: 'GET',
        dataType: 'json',
        error: function(jqXHR, textStatus, errorThrown) {
          return console.log("AJAX Error: " + textStatus);
        },
        success: function(data, textStatus, jqXHR) {
          return initThisMap(thisPlaceMapDom, data);
        }
      });
    }
    owlOptions = {
      items: 1,
      loop: false,
      dots: false,
      nav: false
    };
    $(".places-photos").owlCarousel(owlOptions);
    mapDom = document.getElementById('map');
    if (mapDom != null) {
      $.ajax('/places.json' + window.location.search, {
        type: 'GET',
        dataType: 'json',
        error: function(jqXHR, textStatus, errorThrown) {
          return console.log("AJAX Error: " + textStatus);
        },
        success: function(data, textStatus, jqXHR) {
          return initMap(mapDom, data);
        }
      });
    }
    map = void 0;
    markers = [];
    initPlaceMap = function(mapDom) {
      var autocomplete, input, options, searchBox;
      map = new google.maps.Map(mapDom, {
        center: lithuaniaCenter,
        zoom: 8
      });
      input = document.getElementById("pac-input");
      searchBox = new google.maps.places.SearchBox(input);
      options = {
        componentRestrictions: {
          country: "lt"
        }
      };
      autocomplete = new google.maps.places.Autocomplete(input, options);
      addMarker();
      map.addListener("bounds_changed", function() {
        return searchBox.setBounds(map.getBounds());
      });
      map.addListener("click", function(e) {
        return addMarker(e.latLng);
      });
      return searchBox.addListener("places_changed", function() {
        var bounds, place, places;
        places = searchBox.getPlaces();
        if (places.length !== 1) {
          return;
        }
        clearMarkers();
        bounds = new google.maps.LatLngBounds();
        place = places[0];
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        markers.push(new google.maps.Marker({
          map: map,
          title: place.name,
          position: place.geometry.location
        }));
        setFormData(place);
        bounds.extend(place.geometry.location);
        return map.fitBounds(bounds);
      });
    };
    setFormData = function(place) {
      var placeForm$;
      placeForm$ = $("[data-behaviour='place']");
      placeForm$.find("#place_name").val(place.name);
      placeForm$.find("#place_longitude").val(place.geometry.location.lng());
      return placeForm$.find("#place_latitude").val(place.geometry.location.lat());
    };
    addMarker = function(location) {
      var latLng, marker, placeForm$;
      clearMarkers();
      if (location != null) {
        marker = new google.maps.Marker({
          position: location,
          map: map
        });
        marker.name = "";
      } else {
        placeForm$ = $("[data-behaviour='place']");
        latLng = {
          lat: parseFloat(placeForm$.find("#place_latitude").val()),
          lng: parseFloat(placeForm$.find("#place_longitude").val())
        };
        marker = new google.maps.Marker({
          position: latLng,
          map: map
        });
        marker.name = placeForm$.find("#place_name").val();
      }
      markers.push(marker);
      marker.geometry = {
        location: marker.position
      };
      return setFormData(marker);
    };
    setMapOnOne = function(map, marker) {
      return marker.setMap(map);
    };
    setMapOnAll = function(map) {
      var i, len, marker, results;
      results = [];
      for (i = 0, len = markers.length; i < len; i++) {
        marker = markers[i];
        results.push(marker.setMap(map));
      }
      return results;
    };
    clearMarkers = function() {
      return setMapOnAll(null);
    };
    deleteMarkers = function() {
      clearMarkers();
      return markers = [];
    };
    placeMapDom = document.getElementById("place_map");
    if (placeMapDom != null) {
      return initPlaceMap(placeMapDom);
    }
  });

  $(document).on("turbolinks:before-cache", function() {
    return $(".places-photos").owlCarousel('destroy');
  });

}).call(this);
