(function() {
  $(document).on("turbolinks:load", function() {
    var article_form, article_reveal, owlOptions;
    String.prototype.empty = function() {
      return this.split(" ").join("").length === 0;
    };
    article_reveal = $("#new_article_reveal");
    article_form = $(article_reveal).find("form");
    $("#new_article_reveal").on("closed.zf.reveal", function(e) {
      var article;
      article = {};
      article.id = $("#article_photo_article_id").val();
      if ((article.id != null) && article.id > 0) {
        return App.articles.get_article(article);
      }
    });
    $("#new_article_submit_btn").on("click", function(e) {
      var body, title;
      e.preventDefault();
      title = article_reveal.find("#article_title");
      body = article_reveal.find("#article_body");
      if (!(title.val().empty() || body.val().empty())) {
        $(this).attr("disabled", "");
        return $(article_form).submit();
      }
    });
    $(article_form).on("submit", function(e) {
      var article, body, title, trip_id, trip_route_id;
      e.preventDefault();
      title = article_reveal.find("#article_title");
      body = article_reveal.find("#article_body");
      trip_route_id = article_reveal.find("#trip_route_id");
      if (trip_route_id == null) {
        trip_id = -1;
      } else {
        trip_id = trip_route_id.val();
      }
      article = {
        title: title.val(),
        body: body.val(),
        type: $("#article_type").val(),
        trip_route_id: trip_id
      };
      App.articles.send_article(article);
      title.val("");
      return body.val("");
    });
    owlOptions = {
      items: 1,
      loop: true,
      dots: false
    };
    return $(".article-photos").owlCarousel(owlOptions);
  });

  $(document).on("turbolinks:before-cache", function() {
    return $(".article-photos").owlCarousel('destroy');
  });

}).call(this);
