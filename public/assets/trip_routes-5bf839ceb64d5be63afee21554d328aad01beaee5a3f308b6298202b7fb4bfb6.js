(function() {
  var calculateAndDisplayRoute, computeTotalDistance, directionsRenderer, directionsService, handleAddTripPlaceBtn, handleRemoveTripPlaceBtn, handleVisitPlaceBtn, initNewTripRouteMap, movePlaceToTripPlaces, moveTripPlaceToPlaces, routePlanListSortable, updatePlacesVal;

  directionsService = void 0;

  directionsRenderer = void 0;

  routePlanListSortable = void 0;

  updatePlacesVal = function() {
    $("#places").val(routePlanListSortable.toArray());
    return calculateAndDisplayRoute(directionsService, directionsRenderer);
  };

  computeTotalDistance = function(result) {
    var i, len, myroute, ref, route_leg, totalDist, totalTime;
    totalDist = 0;
    totalTime = 0;
    myroute = result.routes[0];
    ref = myroute.legs;
    for (i = 0, len = ref.length; i < len; i++) {
      route_leg = ref[i];
      totalDist += route_leg.distance.value;
      totalTime += route_leg.duration.value;
    }
    totalDist = (totalDist / 1000).toFixed(2);
    totalTime = (totalTime / 60).toFixed(2);
    $(".duration").val(totalTime).html(totalTime);
    return $(".distance").val(totalDist).html(totalDist);
  };

  calculateAndDisplayRoute = function(directionsService, directionsRenderer) {
    var destinationLoc, origingLoc, wayPoints;
    wayPoints = [];
    $.each($("ul#route-plan-list").find(".place__info"), function(index, el) {
      return wayPoints.push({
        location: {
          lat: $(el).data("place-position-lat"),
          lng: $(el).data("place-position-lng")
        },
        stopover: true
      });
    });
    console.log(wayPoints);
    if (wayPoints.length >= 2) {
      origingLoc = wayPoints.shift();
      destinationLoc = wayPoints.pop();
      return directionsService.route({
        origin: origingLoc.location,
        destination: destinationLoc.location,
        waypoints: wayPoints,
        optimizeWaypoints: false,
        travelMode: 'DRIVING'
      }, function(response, status) {
        var route;
        if (status === 'OK') {
          directionsRenderer.setDirections(response);
          route = response.routes[0];
          return computeTotalDistance(response);
        } else {
          return window.alert('Directions request failed due to ' + status);
        }
      });
    }
  };

  initNewTripRouteMap = function(newTripRouteMap) {
    newTripRouteMap = new google.maps.Map($(newTripRouteMap).get(0), {
      zoom: 6,
      center: {
        lat: 55.147614,
        lng: 24.4555564
      }
    });
    directionsRenderer.setMap(newTripRouteMap);
    return calculateAndDisplayRoute(directionsService, directionsRenderer);
  };

  movePlaceToTripPlaces = function(placeObj, routePlanningTable$) {
    var routePlaceHtml;
    routePlaceHtml = "<li class=\"list-group-item place__info route-plan-item\"\n    data-place-name=\"" + placeObj.name + "\"\n    data-place-id=\"" + placeObj.id + "\"\n    data-place-type-id=\"" + placeObj.place_type_id + "\"\n    data-place-position-lat=\"" + placeObj.position.lat + "\"\n    data-place-position-lng=\"" + placeObj.position.lng + "\">\n  <div class=\"row\">\n    <div class=\"small-2 columns plan-item-handler\">\n      <span><i class=\"fi-paw medium\"></i></span>\n    </div>\n    <div class=\"small-8 columns place-name\">\n      " + placeObj.name + "\n    </div>\n    <div class=\"small-2 columns plan-item-remove\">\n      <button data-behaviour=\"remove-selected-place\"><i class=\"fi-x small\"></i></button>\n    </div>\n  </div>\n</li>";
    $(routePlanningTable$).append(routePlaceHtml);
    return $(routePlanningTable$).find("[data-place-id='" + placeObj.id + "'] [data-behaviour='remove-selected-place']").on("click", handleRemoveTripPlaceBtn);
  };

  moveTripPlaceToPlaces = function(place, placesTypeSelect$) {
    var placeActionsHtml;
    placeActionsHtml = "<div class=\"small-6 columns place-info\"\n  data-place-name=\"" + place.name + "\"\n  data-place-id=\"" + place.id + "\"\n  data-place-type-id=\"" + place.place_type_id + "\"\n  data-place-position-lat=\"" + place.position.lat + "\"\n  data-place-position-lng=\"" + place.position.lng + "\">\n  <div class=\"card\">\n    <div class=\"card-section\">\n      <div class=\"row columns\">\n        " + (place.name.split(' ')[0]) + "\n      </div>\n      <div class=\"row\">\n        <div class=\"columns\">\n          <button data-behaviour=\"add__place\">\n            <i class=\"fi-check\"></i>\n          </button>  \n        </div>\n        <div class=\"columns\">\n          <button>\n            <i class=\"fi-x\"></i>\n          </button>  \n        </div>\n        <div class=\"columns\">\n          <button>\n            <i class=\"fi-list\"></i>\n          </button>  \n        </div>\n      </div>\n    </div>\n  </div>\n</div>";
    placesTypeSelect$.prepend(placeActionsHtml);
    return placesTypeSelect$.find("[data-place-id='" + place.id + "'] [data-behaviour='add__place']").on("click", handleAddTripPlaceBtn);
  };

  handleAddTripPlaceBtn = function(e) {
    var placeInfo$, placeObj, routePlanList$;
    routePlanList$ = $("ul#route-plan-list");
    placeInfo$ = $(this).closest(".place-info");
    placeObj = {
      id: $(placeInfo$).data("place-id"),
      name: $(placeInfo$).data("place-name"),
      place_type_id: $(placeInfo$).data("place-type-id"),
      position: {
        lat: $(placeInfo$).data("place-position-lat"),
        lng: $(placeInfo$).data("place-position-lng")
      }
    };
    movePlaceToTripPlaces(placeObj, routePlanList$);
    placeInfo$.remove();
    return updatePlacesVal();
  };

  handleRemoveTripPlaceBtn = function(e) {
    var placeInfo$, placeObj, placeType$, placeTypes$;
    placeTypes$ = $(".place-type");
    placeInfo$ = $(this).closest(".route-plan-item");
    placeObj = {
      id: $(placeInfo$).data("place-id"),
      name: $(placeInfo$).data("place-name"),
      place_type_id: Number($(placeInfo$).data("place-type-id")),
      position: {
        lat: $(placeInfo$).data("place-position-lat"),
        lng: $(placeInfo$).data("place-position-lng")
      }
    };
    placeType$ = void 0;
    $.grep(placeTypes$, function(el) {
      if ($(el).data("place-type-id") === placeObj.place_type_id) {
        return placeType$ = el;
      }
    });
    if (placeType$ != null) {
      moveTripPlaceToPlaces(placeObj, $(placeType$).find(".place-type-content .place-type-places"));
      $(placeInfo$).remove();
      return updatePlacesVal();
    }
  };

  handleVisitPlaceBtn = function(e) {
    var place;
    e.preventDefault();
    place = {
      id: $(this).data("place-id"),
      visited: $(this).data("place-visited"),
      selected_route_id: $(this).data("selected-route-id")
    };
    App.trip_route.update_trip_route_place(place);
    return $(this).closest(".reveal").foundation("close");
  };

  $(document).on("turbolinks:load", function() {
    var form$, newTripRouteMap, placeTypes$, routePlanList$;
    routePlanList$ = $("ul#route-plan-list");
    placeTypes$ = $(".place-type");
    placeTypes$.find("[data-behaviour='add__place']").on("click", handleAddTripPlaceBtn);
    routePlanList$.find("[data-behaviour='remove-selected-place']").on("click", handleRemoveTripPlaceBtn);
    if ($(routePlanList$).length > 0) {
      routePlanListSortable = Sortable.create(document.getElementById($(routePlanList$).attr("id")), {
        dataIdAttr: "data-place-id",
        handle: ".fi-paw",
        animation: 150,
        onUpdate: function(evt) {
          return updatePlacesVal();
        }
      });
    }
    newTripRouteMap = $("#new_trip_route_map");
    if (newTripRouteMap.length > 0) {
      directionsService = new google.maps.DirectionsService;
      directionsRenderer = new google.maps.DirectionsRenderer;
      initNewTripRouteMap(newTripRouteMap);
    }
    form$ = $("form[data-behaviour='trip-form']");
    form$.find("#submit__button").on("click", function(event) {
      updatePlacesVal();
      return form$.submit();
    });
    $("[data-behaviour='update-trip-places']").on("click", handleVisitPlaceBtn);
    return window.handleVisitPlaceBtn = handleVisitPlaceBtn;
  });

}).call(this);
