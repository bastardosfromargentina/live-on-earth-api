(function() {
  $(document).on("turbolinks:load", function() {
    var dropzones;
    dropzones = $("[data-behaviour='new-photo']").addClass("dropzone");
    $(dropzones).dropzone({
      url: $(this).attr("action"),
      maxFilesize: 2,
      paramName: $(this).find("input[type='file']").attr("name"),
      addRemoveLinks: true,
      success: function(file, response) {
        $(file.previewTemplate).find('.dz-remove').attr('id', response.fileID);
        $(file.previewElement).addClass("dz-success");
        window.dropp = this;
        return window.filee = file;
      },
      removedfile: function(file) {
        var _file, _this, id;
        _this = this;
        _file = file;
        id = $(file.previewTemplate).find('.dz-remove').attr('id');
        return $.ajax({
          type: 'DELETE',
          url: $(dropzones).attr("action") + "/" + id,
          success: function(data) {
            file.previewElement.remove();
            return file = null;
          }
        });
      }
    });
    return $("[data-behaviour='remove-photo']").on("click", function(e) {
      var _this, id;
      _this = this;
      id = $(this).data('photo-id');
      return $.ajax({
        type: 'DELETE',
        url: $(dropzones).attr("action") + "/" + id,
        success: function(data) {
          return $(_this).closest("[data-behaviour='photo-wrapper']").remove();
        }
      });
    });
  });

}).call(this);
