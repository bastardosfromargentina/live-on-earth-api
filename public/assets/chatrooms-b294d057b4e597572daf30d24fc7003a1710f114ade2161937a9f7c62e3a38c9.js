(function() {
  var handleVisiblityChange;

  handleVisiblityChange = function() {
    var $strike, chatroom_id;
    $strike = $(".strike");
    if ($strike.length > 0) {
      chatroom_id = $("[data-behavior='messages']").data("chatroom-id");
      App.last_read.update(chatroom_id);
      return $strike.remove();
    }
  };

  $.fn.isolatedScroll = function() {
    return this.on('mousewheel DOMMouseScroll', function(e) {
      var bottomOverflow, delta, topOverflow;
      delta = e.wheelDelta || e.originalEvent && e.originalEvent.wheelDelta || -e.detail;
      bottomOverflow = this.scrollTop + $(this).outerHeight() - this.scrollHeight >= 0;
      topOverflow = this.scrollTop <= 0;
      if (delta < 0 && bottomOverflow || delta > 0 && topOverflow) {
        return e.preventDefault();
      }
    });
  };

  $.fn.scrolledDown = function() {
    return this.scrollTop($(this)[0].scrollHeight);
  };

  $.fn.initChatroomTab = function() {
    var chatForm;
    $(this).find("ul").isolatedScroll().scrolledDown();
    chatForm = $(this).find("#new_message");
    $(chatForm).on("keypress", function(e) {
      if (e && e.keyCode === 13) {
        e.preventDefault();
        return $(this).submit();
      }
    });
    return $(chatForm).on("submit", function(e) {
      var body, chatroom_id;
      e.preventDefault();
      chatroom_id = $(this).closest("[data-behavior='messages']").data("chatroom-id");
      body = $(this).find("#message_body");
      console.log(chatroom_id + " - --" + body.val());
      App.chatrooms.send_message(chatroom_id, body.val());
      return body.val("");
    });
  };

  $(document).on("turbolinks:load", function() {
    var chatroomReveal$, chatroomsContent$;
    $(document).on("click", handleVisiblityChange);
    chatroomsContent$ = $(".chatroom-content");
    if (chatroomsContent$.length > 0) {
      chatroomsContent$.initChatroomTab();
    }
    chatroomReveal$ = $(".chatroom-reveal");
    if (chatroomReveal$.length > 0) {
      return chatroomReveal$.initChatroomTab();
    }
  });

}).call(this);
