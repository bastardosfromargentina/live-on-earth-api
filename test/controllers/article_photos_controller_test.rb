require 'test_helper'

class ArticlePhotosControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get article_photos_index_url
    assert_response :success
  end

  test "should get create" do
    get article_photos_create_url
    assert_response :success
  end

end
