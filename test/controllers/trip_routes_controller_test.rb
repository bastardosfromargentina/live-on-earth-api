require 'test_helper'

class TripRoutesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @trip_route = trip_routes(:one)
  end

  test "should get index" do
    get trip_routes_url
    assert_response :success
  end

  test "should get new" do
    get new_trip_route_url
    assert_response :success
  end

  test "should create trip_route" do
    assert_difference('TripRoute.count') do
      post trip_routes_url, params: { trip_route: { name: @trip_route.name, user_id: @trip_route.user_id } }
    end

    assert_redirected_to trip_route_url(TripRoute.last)
  end

  test "should show trip_route" do
    get trip_route_url(@trip_route)
    assert_response :success
  end

  test "should get edit" do
    get edit_trip_route_url(@trip_route)
    assert_response :success
  end

  test "should update trip_route" do
    patch trip_route_url(@trip_route), params: { trip_route: { name: @trip_route.name, user_id: @trip_route.user_id } }
    assert_redirected_to trip_route_url(@trip_route)
  end

  test "should destroy trip_route" do
    assert_difference('TripRoute.count', -1) do
      delete trip_route_url(@trip_route)
    end

    assert_redirected_to trip_routes_url
  end
end
