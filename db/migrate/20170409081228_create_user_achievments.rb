class CreateUserAchievments < ActiveRecord::Migration[5.0]
  def change
    create_table :user_achievments do |t|
      t.integer :user_id
      t.integer :achievment_id

      t.timestamps
    end

    add_index :user_achievments, :user_id 
    add_foreign_key :user_achievments, :users, column: :user_id
    add_index :user_achievments, :achievment_id 
    add_foreign_key :user_achievments, :achievments, column: :achievment_id   
  end
end
