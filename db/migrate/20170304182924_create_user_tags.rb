class CreateUserTags < ActiveRecord::Migration[5.0]
  def change
    create_table :user_tags do |t|
      t.integer :tag_id
      t.integer :user_id

      t.timestamps
    end
    add_index :user_tags, :user_id 
    add_foreign_key :user_tags, :users, column: :user_id
    add_index :user_tags, :tag_id 
    add_foreign_key :user_tags, :tags, column: :tag_id
  end
end
