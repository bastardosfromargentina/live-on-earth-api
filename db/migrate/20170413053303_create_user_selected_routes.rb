class CreateUserSelectedRoutes < ActiveRecord::Migration[5.0]
  def change
    create_table :user_selected_routes do |t|
      t.integer :trip_route_id
      t.integer :user_id
      t.hstore :places, default: {}, null: false

      t.timestamps
    end
    add_index :user_selected_routes, :trip_route_id 
    add_foreign_key :user_selected_routes, :trip_routes, column: :trip_route_id
    add_index :user_selected_routes, :user_id 
    add_foreign_key :user_selected_routes, :users, column: :user_id
  end
end
