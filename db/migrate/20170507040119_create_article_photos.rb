class CreateArticlePhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :article_photos do |t|
      t.integer :article_id
      t.attachment :image

      t.timestamps
    end
    add_index :article_photos, :article_id 
    add_foreign_key :article_photos, :articles, column: :article_id
  end
end
