class CreateChatrooms < ActiveRecord::Migration[5.0]
  def change
    create_table :chatrooms do |t|
      t.string :title
      t.date :last_read_at

      t.timestamps
    end
  end
end
