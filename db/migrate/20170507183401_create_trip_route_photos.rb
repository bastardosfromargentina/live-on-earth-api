class CreateTripRoutePhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :trip_route_photos do |t|
      t.integer :trip_route_id
      t.integer :photo_id

      t.timestamps
    end
    add_index :trip_route_photos, :trip_route_id 
    add_foreign_key :trip_route_photos, :trip_routes, column: :trip_route_id
    add_index :trip_route_photos, :photo_id 
    add_foreign_key :trip_route_photos, :photos, column: :photo_id
  end
end
