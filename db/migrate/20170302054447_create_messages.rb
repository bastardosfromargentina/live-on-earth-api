class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.text :body
      t.integer :user_id
      t.integer :chatroom_id

      t.timestamps
    end
    add_index :messages, :user_id 
    add_foreign_key :messages, :users, column: :user_id
    add_index :messages, :chatroom_id 
    add_foreign_key :messages, :chatrooms, column: :chatroom_id
  end
end
