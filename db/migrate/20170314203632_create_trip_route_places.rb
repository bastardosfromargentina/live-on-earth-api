class CreateTripRoutePlaces < ActiveRecord::Migration[5.0]
  def change
    create_table :trip_route_places do |t|
      t.integer :trip_route_id
      t.integer :place_id
      t.integer :waypoint_nr
      t.boolean :visited, default: false
      t.timestamps
    end
    add_index :trip_route_places, :trip_route_id 
    add_foreign_key :trip_route_places, :trip_routes, column: :trip_route_id
    add_index :trip_route_places, :place_id 
    add_foreign_key :trip_route_places, :places, column: :place_id
  end
    
end
