class CreateChatroomUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :chatroom_users do |t|
      t.datetime :last_read_at
      t.integer :user_id
      t.integer :chatroom_id

      t.timestamps
    end
    add_index :chatroom_users, :user_id 
    add_foreign_key :chatroom_users, :users, column: :user_id
    add_index :chatroom_users, :chatroom_id 
    add_foreign_key :chatroom_users, :chatrooms, column: :chatroom_id
  end
end
