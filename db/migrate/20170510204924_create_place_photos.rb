class CreatePlacePhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :place_photos do |t|
      t.integer :place_id
      t.attachment :image

      t.timestamps
    end
    add_index :place_photos, :place_id 
    add_foreign_key :place_photos, :places, column: :place_id
  end
end
