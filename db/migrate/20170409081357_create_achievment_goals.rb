class CreateAchievmentGoals < ActiveRecord::Migration[5.0]
  def change
    create_table :achievment_goals do |t|
      t.integer :achievment_id
      t.integer :variable_id
      t.integer :goal

      t.timestamps
    end    
    add_index :achievment_goals, :achievment_id 
    add_foreign_key :achievment_goals, :achievments, column: :achievment_id
    add_index :achievment_goals, :variable_id 
    add_foreign_key :achievment_goals, :variables, column: :variable_id   
  end
end
