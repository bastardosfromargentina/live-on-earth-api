class CreateTripRoutes < ActiveRecord::Migration[5.0]
  def change
    create_table :trip_routes do |t|
      t.string :name
      t.integer :user_id
      t.text :description
      t.timestamps
    end

  add_index :trip_routes, :user_id 
  add_foreign_key :trip_routes, :users, column: :user_id    
  end
end
