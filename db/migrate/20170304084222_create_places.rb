class CreatePlaces < ActiveRecord::Migration[5.0]
  def change
    create_table :places do |t|
      t.string :name
      t.text :description
      t.float :longitude
      t.float :latitude
      t.integer :user_id
      t.integer :place_type_id

      t.timestamps
    end
    add_index :places, :user_id 
    add_foreign_key :places, :users, column: :user_id
    add_index :places, :place_type_id 
    add_foreign_key :places, :place_types, column: :place_type_id
  end
end
