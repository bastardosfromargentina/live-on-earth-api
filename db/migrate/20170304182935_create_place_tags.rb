class CreatePlaceTags < ActiveRecord::Migration[5.0]
  def change
    create_table :place_tags do |t|
      t.integer :tag_id
      t.integer :place_id

      t.timestamps
    end
    add_index :place_tags, :place_id 
    add_foreign_key :place_tags, :places, column: :place_id
    add_index :place_tags, :tag_id 
    add_foreign_key :place_tags, :tags, column: :tag_id
  end
end
