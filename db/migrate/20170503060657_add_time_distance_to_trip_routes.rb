class AddTimeDistanceToTripRoutes < ActiveRecord::Migration[5.0]
  def change
    add_column :trip_routes, :distance, :integer
    add_column :trip_routes, :duration, :integer 
  end
end
