class CreateUserPhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :user_photos do |t|
      t.integer :user_id
      t.integer :photo_id

      t.timestamps
    end
    add_index :user_photos, :user_id 
    add_foreign_key :user_photos, :users, column: :user_id
    add_index :user_photos, :photo_id 
    add_foreign_key :user_photos, :photos, column: :photo_id
  end
end
