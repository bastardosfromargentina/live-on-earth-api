class CreateArticleComments < ActiveRecord::Migration[5.0]
  def change
    create_table :article_comments do |t|
      t.integer :article_id
      t.integer :comment_id

      t.timestamps
    end
    add_index :article_comments, :article_id 
    add_foreign_key :article_comments, :articles, column: :article_id
    add_index :article_comments, :comment_id 
    add_foreign_key :article_comments, :comments, column: :comment_id
  end
end
