class CreateUserVariables < ActiveRecord::Migration[5.0]
  def change
    create_table :user_variables do |t|
      t.integer :user_id
      t.integer :variable_id
      t.integer :value, default: 0

      t.timestamps
    end    
    add_index :user_variables, :user_id 
    add_foreign_key :user_variables, :users, column: :user_id
    add_index :user_variables, :variable_id 
    add_foreign_key :user_variables, :variables, column: :variable_id    
  end
end
