class CreateTripRouteArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :trip_route_articles do |t|
      t.integer :trip_route_id
      t.integer :article_id

      t.timestamps
    end
    add_index :trip_route_articles, :article_id 
    add_foreign_key :trip_route_articles, :articles, column: :article_id
    add_index :trip_route_articles, :trip_route_id 
    add_foreign_key :trip_route_articles, :trip_routes, column: :trip_route_id
  end
end
