# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170510204924) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "achievment_goals", force: :cascade do |t|
    t.integer  "achievment_id"
    t.integer  "variable_id"
    t.integer  "goal"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["achievment_id"], name: "index_achievment_goals_on_achievment_id", using: :btree
    t.index ["variable_id"], name: "index_achievment_goals_on_variable_id", using: :btree
  end

  create_table "achievments", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "article_comments", force: :cascade do |t|
    t.integer  "article_id"
    t.integer  "comment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id"], name: "index_article_comments_on_article_id", using: :btree
    t.index ["comment_id"], name: "index_article_comments_on_comment_id", using: :btree
  end

  create_table "article_photos", force: :cascade do |t|
    t.integer  "article_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["article_id"], name: "index_article_photos_on_article_id", using: :btree
  end

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.text     "body"
    t.integer  "topic"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_articles_on_user_id", using: :btree
  end

  create_table "chatroom_users", force: :cascade do |t|
    t.datetime "last_read_at"
    t.integer  "user_id"
    t.integer  "chatroom_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["chatroom_id"], name: "index_chatroom_users_on_chatroom_id", using: :btree
    t.index ["user_id"], name: "index_chatroom_users_on_user_id", using: :btree
  end

  create_table "chatrooms", force: :cascade do |t|
    t.string   "title"
    t.date     "last_read_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_comments_on_user_id", using: :btree
  end

  create_table "friendships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.integer  "status",     default: 0
    t.string   "key_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["friend_id"], name: "index_friendships_on_friend_id", using: :btree
    t.index ["user_id"], name: "index_friendships_on_user_id", using: :btree
  end

  create_table "messages", force: :cascade do |t|
    t.text     "body"
    t.integer  "user_id"
    t.integer  "chatroom_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["chatroom_id"], name: "index_messages_on_chatroom_id", using: :btree
    t.index ["user_id"], name: "index_messages_on_user_id", using: :btree
  end

  create_table "photos", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "place_photos", force: :cascade do |t|
    t.integer  "place_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["place_id"], name: "index_place_photos_on_place_id", using: :btree
  end

  create_table "place_tags", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "place_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["place_id"], name: "index_place_tags_on_place_id", using: :btree
    t.index ["tag_id"], name: "index_place_tags_on_tag_id", using: :btree
  end

  create_table "place_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "places", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.float    "longitude"
    t.float    "latitude"
    t.integer  "user_id"
    t.integer  "place_type_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["place_type_id"], name: "index_places_on_place_type_id", using: :btree
    t.index ["user_id"], name: "index_places_on_user_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trip_route_articles", force: :cascade do |t|
    t.integer  "trip_route_id"
    t.integer  "article_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["article_id"], name: "index_trip_route_articles_on_article_id", using: :btree
    t.index ["trip_route_id"], name: "index_trip_route_articles_on_trip_route_id", using: :btree
  end

  create_table "trip_route_photos", force: :cascade do |t|
    t.integer  "trip_route_id"
    t.integer  "photo_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["photo_id"], name: "index_trip_route_photos_on_photo_id", using: :btree
    t.index ["trip_route_id"], name: "index_trip_route_photos_on_trip_route_id", using: :btree
  end

  create_table "trip_route_places", force: :cascade do |t|
    t.integer  "trip_route_id"
    t.integer  "place_id"
    t.integer  "waypoint_nr"
    t.boolean  "visited",       default: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["place_id"], name: "index_trip_route_places_on_place_id", using: :btree
    t.index ["trip_route_id"], name: "index_trip_route_places_on_trip_route_id", using: :btree
  end

  create_table "trip_routes", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "distance"
    t.integer  "duration"
    t.index ["user_id"], name: "index_trip_routes_on_user_id", using: :btree
  end

  create_table "user_achievments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "achievment_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["achievment_id"], name: "index_user_achievments_on_achievment_id", using: :btree
    t.index ["user_id"], name: "index_user_achievments_on_user_id", using: :btree
  end

  create_table "user_photos", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "photo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["photo_id"], name: "index_user_photos_on_photo_id", using: :btree
    t.index ["user_id"], name: "index_user_photos_on_user_id", using: :btree
  end

  create_table "user_selected_routes", force: :cascade do |t|
    t.integer  "trip_route_id"
    t.integer  "user_id"
    t.hstore   "places",        default: {}, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["trip_route_id"], name: "index_user_selected_routes_on_trip_route_id", using: :btree
    t.index ["user_id"], name: "index_user_selected_routes_on_user_id", using: :btree
  end

  create_table "user_tags", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tag_id"], name: "index_user_tags_on_tag_id", using: :btree
    t.index ["user_id"], name: "index_user_tags_on_user_id", using: :btree
  end

  create_table "user_variables", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "variable_id"
    t.integer  "value",       default: 0
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["user_id"], name: "index_user_variables_on_user_id", using: :btree
    t.index ["variable_id"], name: "index_user_variables_on_variable_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "name"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "variables", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_foreign_key "achievment_goals", "achievments"
  add_foreign_key "achievment_goals", "variables"
  add_foreign_key "article_comments", "articles"
  add_foreign_key "article_comments", "comments"
  add_foreign_key "article_photos", "articles"
  add_foreign_key "articles", "users"
  add_foreign_key "chatroom_users", "chatrooms"
  add_foreign_key "chatroom_users", "users"
  add_foreign_key "comments", "users"
  add_foreign_key "friendships", "users"
  add_foreign_key "friendships", "users", column: "friend_id"
  add_foreign_key "messages", "chatrooms"
  add_foreign_key "messages", "users"
  add_foreign_key "place_photos", "places"
  add_foreign_key "place_tags", "places"
  add_foreign_key "place_tags", "tags"
  add_foreign_key "places", "place_types"
  add_foreign_key "places", "users"
  add_foreign_key "trip_route_articles", "articles"
  add_foreign_key "trip_route_articles", "trip_routes"
  add_foreign_key "trip_route_photos", "photos"
  add_foreign_key "trip_route_photos", "trip_routes"
  add_foreign_key "trip_route_places", "places"
  add_foreign_key "trip_route_places", "trip_routes"
  add_foreign_key "trip_routes", "users"
  add_foreign_key "user_achievments", "achievments"
  add_foreign_key "user_achievments", "users"
  add_foreign_key "user_photos", "photos"
  add_foreign_key "user_photos", "users"
  add_foreign_key "user_selected_routes", "trip_routes"
  add_foreign_key "user_selected_routes", "users"
  add_foreign_key "user_tags", "tags"
  add_foreign_key "user_tags", "users"
  add_foreign_key "user_variables", "users"
  add_foreign_key "user_variables", "variables"
end
