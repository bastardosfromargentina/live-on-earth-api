Tag.create!([{name: 'bokstai'}, {name: 'viduramziai'}, {name: 'gamta'}, {name: 'kaunas'}, {name: 'vilnius'}])

User.create!([
                 {
                     name: 'pucinsk', email: 'pucinsk@pucinsk.com',
                     password: 'pucinsk',
                     password_confirmation: 'pucinsk'
                 }, {
                     name: 'jacob', email: 'elc@yma.com',
                     password: 'sabukoj',
                     password_confirmation: 'sabukoj'
                 }, {
                     name: 'jacob1', email: 'yma@yma.com',
                     password: 'sabukoj',
                     password_confirmation: 'sabukoj'
                 }
             ])

(0..10).to_a.each do |i|
  pass = Faker::Internet.password(10, 20)
  User.create!(name: Faker::Pokemon.name, email: "#{i.to_s}#{Faker::Internet.safe_email}",
               password: pass,
               password_confirmation: pass)
end

Variable.create!([
  {name: 'created_routes', description: 'Sukurti maršrutai'},
  {name: 'completed_routes', description: 'Įveikti maršrutai'},
  {name: 'created_places', description: 'Pridėtos vietos'}
])

Achievment.create!([
  {name: 'first_created_route', description: 'Pirmas sukurtas maršrutas'},
  {name: 'five_created_routes', description: 'Penki sukurti maršrutai'},
  {name: 'first_completed_route', description: 'Pirmas įveiktas maršrutas'},
  {name: 'five_completed_routes', description: 'Penki įveikti maršrutai'},
  {name: 'first_created_place', description: 'Pirma sukurta lankytina vieta'},
  {name: 'five_created_places', description: 'Penkios pridėtos lankytinos vietos'}
])

AchievmentGoal.create!([
  {achievment: Achievment.find_by_name('first_created_route'), variable: Variable.find_by_name('created_routes'), goal: 1},
  {achievment: Achievment.find_by_name('five_created_routes'), variable: Variable.find_by_name('created_routes'), goal: 5},
  {achievment: Achievment.find_by_name('first_completed_route'), variable: Variable.find_by_name('completed_routes'), goal: 1},
  {achievment: Achievment.find_by_name('five_completed_routes'), variable: Variable.find_by_name('completed_routes'), goal: 5},
  {achievment: Achievment.find_by_name('first_created_place'), variable: Variable.find_by_name('created_places'), goal: 1},
  {achievment: Achievment.find_by_name('five_created_places'), variable: Variable.find_by_name('created_places'), goal: 5}
])

PlaceType.create([
                     {name: 'apžvalgos bokštai'},
                     {name: 'pažintiniai takai'},
                     {name: 'muziejai'},
                     {name: 'religiniai objektai'},
                     {name: 'gamtos objektai'},
                     {name: 'dvarai ir pilys'},
                     {name: 'kita'}
                 ])

Place.create!([{
                   name: "Lajų tako bokštas (33 m)",
                   description: "",
                   longitude: 25.0603455,
                   latitude: 55.4855836,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Šiliniškių (Ginučių) apžvalgos bokštas (\"Bitės\") (30 m)",
                   description: "",
                   longitude: 25.9743887,
                   latitude: 55.3812073,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Vilniaus TV bokštas",
                   description: "TV bokštas yra vienas iš aukščiausių Rytų Europos statinių. Aukštis – 326,5 m. Jame įrengta Sausio 13-osios muziejinė ekspozicija – Laisvės kovų muziejus, restoranas “Paukščių takas”. Apžvalgos aikštelė 165 m aukštyje.",
                   longitude: 25.214921200000003,
                   latitude: 54.6871606,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Etnokosmologijos muziejaus apžvalgos aikštelė (32 m)",
                   description: "",
                   longitude: 25.5551541,
                   latitude: 55.315193,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Rusnės apžvalgos bokštelis",
                   description: "",
                   longitude: 21.2803835,
                   latitude: 55.3263216,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Kalnalio apžvalgos bokštas (15 m)",
                   description: "",
                   longitude: 21.5414214,
                   latitude: 56.0171271,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Rubikių apžvalgos bokštas (15 m)",
                   description: "",
                   longitude: 25.267868,
                   latitude: 55.4837507,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Ventos ir Virvytės santakos bokštas",
                   description: "Dar kitaip - Santeklių bokštas.",
                   longitude: 22.5511551,
                   latitude: 56.2304303,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Aukštagirės apžvalgos bokštas (15 m)",
                   description: "",
                   longitude: 22.3278531,
                   latitude: 55.6125821,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Ignalinos (Vilkakalnio) apžvalgos bokštas (26 m)",
                   description: "",
                   longitude: 26.1780649,
                   latitude: 55.3347742,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Čepkelių raisto apžvalgos bokštas",
                   description: "Patekti galima tik su Čepkelių gamtinio rezervato direkcijos leidimu.",
                   longitude: 24.4289589,
                   latitude: 54.0212752,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Rumšiškių apžvalgos bokštas (15 m)",
                   description: "",
                   longitude: 24.1924551,
                   latitude: 54.8590708,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Puvočių apžvalgos bokštas (30 m)",
                   description: "\"Omnitel\" bokšte įrengta apžvalgos aikštelė.",
                   longitude: 24.30926,
                   latitude: 54.1141834,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Rudesos (Baltųjų Lakajų) apžvalgos bokštas (15 m)",
                   description: "Pats bokštas - 35 m, bet aikštelė įrengta 15 m aukštyje. Lankymas su Labanoro regioninio parko lankytojų centro darbuotojais iš anksto susitarus.",
                   longitude: 25.5359817,
                   latitude: 55.2149416,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Senadvario (Švenčionių) apžvalgos bokštas (26 m)",
                   description: "",
                   longitude: 26.2101173,
                   latitude: 55.1654334,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Sartų ežero apžvalgos bokštas (33 m)",
                   description: "",
                   longitude: 25.8385858,
                   latitude: 55.8409567,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Tytuvėnų apžvalgos bokštas (15 m)",
                   description: "",
                   longitude: 23.2401127,
                   latitude: 55.5898555,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Uostadvario švyturys (18 m)",
                   description: "Uostadvario švyturys yra taisyklingo aštuonkampio formos, sienų briaunos papuoštos glazūruotomis žalios spalvos plytomis, sienos raudonų plytų mūro. Viduje – įvijiniai betoniniai laiptai, vedantys į žibinto patalpą, iš kurios patenkama į apžvalgos aikštelę. Švyturio aukštis – 18 metrų. 1873 – 1876 metais priešais Minijos žiotis pastatytas Uostadvario švyturys , sujungtas su švyturio sargo gyvenamuoju namu. Dabar tai technikos istorijos paminklas, navigacijai nebenaudojamas, bet yra puikus Rusnės ir Minijos apylinkių apžvalgos bokštas.",
                   longitude: 21.2913913,
                   latitude: 55.3442287,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Siberijos apžvalgos bokštas (15 m)",
                   description: "",
                   longitude: 21.814553400000005,
                   latitude: 56.0303958,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Kirkilų apžvalgos bokštas (30 m)",
                   description: "",
                   longitude: 24.6907425,
                   latitude: 56.2478716,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Krekenavos regioninio parko bokštas (25 m)",
                   description: "",
                   longitude: 24.105565,
                   latitude: 55.551819,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Papilės (Jurakalnio) apžvalgos bokštas (15 m)",
                   description: "Šalia Jurakalnio geologinės atodangos ir atragio stoavi 15 m apžvalgos bokštas. Nuo jo atsiveria nuostabus vaizdas į išraiškingą Ventos upės slėnį, Papilės miestelį bei lankytinus objektus: Jurakalnio geologinę atodangą ir atragį, Papilės I ir II piliakalnius, Šv. Juozapo bažnyčią, Penkiolikakamienę liepą ir kt.",
                   longitude: 22.7830669,
                   latitude: 56.1447068,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Lygumų apžvalgos bokštas",
                   description: "Lygumų kalnas yra pietiniame Vajuonio ežero krante. Ši, trijų ežerų supama, 180 m aukščio virš jūros lygio siekianti plokštikalnė yra tarsi milžiniška apžvalgos aikštelė. Į šiaurę tyvuliuoja Vajuonis. Į pietus nuo Akmeniškių kaimo atsiveria panorama į Kretuonykštį. Vieta pasirinkta neatsitiktinai – iš čia visas Kretuono ežeras matosi kaip ant delno. Kalno papėdėje auganti senų medžių alėja žymi kadaise čia buvusio dvaro vietą. Apžvalgos aikštelė įrengta 15 m aukštyje.",
                   longitude: 26.1223501,
                   latitude: 55.2704307,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Drevernos apžvalgos bokštas (15 m)",
                   description: "",
                   longitude: 21.2336084,
                   latitude: 55.516224,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Katedros varpinė",
                   description: "Apžvalgos aikštelė - 50 m aukštyje.",
                   longitude: 25.286707900000003,
                   latitude: 54.6856846,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Šv. Jonų varpinė",
                   description: "",
                   longitude: 25.2881777,
                   latitude: 54.6823478,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Mindūnų (Labanoro) apžvalgos bokštas (36 m)",
                   description: "",
                   longitude: 25.5608833,
                   latitude: 55.2192565,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Metelių apžvalgos bokštas (15 m)",
                   description: "Nuo bokšto galima apžvelgti vertingiausius Metelių kraštovaizdžio draustinio objektus – Metelio ežerą, paukščiams svarbią šlapią Kemsinės pievą, Metelių kaimą, kitoje ežero pusėje stūksantį Papėčių piliakalnį. Matomas siauras Dusios ežero ruožas.",
                   longitude: 23.7426192,
                   latitude: 54.2955576,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Didžiasalio apžvalgos bokštas ",
                   description: "12,5 m aukščio.",
                   longitude: 26.1843628,
                   latitude: 55.2290276,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Snaigyno ežero apžvalgos bokštas (15 m)",
                   description: "",
                   longitude: 23.7221244,
                   latitude: 54.1013379,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Kamanų rezervato bokštas (30 m)",
                   description: "Bokštą yra rezervato teritorijoje, todėl jo lankymas įmanomas tik su gidu, iš anksto susiderinus su rezervato administracija.<br>Ekskursiją mokomuoju taku ir gamtinės ekspozicijos apžiūrą būtina užsisakyti iš anksto paskambinus į direkcija tel. 8 425 59285 arba el. paštu: info@kamanos.lt. Lankytojų centro apžiūra užtrunka ~ 1val., ekskursija mokomuoju taku - ~2-3 val.",
                   longitude: 22.6519364,
                   latitude: 56.3131866,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Merkinės apžvalgos bokštas (26 m)",
                   description: "",
                   longitude: 24.1746587,
                   latitude: 54.1637845,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Rusnės regykla",
                   description: "",
                   longitude: 21.2970239,
                   latitude: 55.3076232,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Ventės rago švyturys",
                   description: "Pirmasis medinis švyturys Ventės rage buvo pastatytas dar 1837 m. Jis buvo apšviečiamas alyva kūrenama lempa. Dabartinis raudonų plytų mūrinis švyturys pastatytas 1852 m. Jo aukštis yra 11 m. Ventės rago švyturys vienas iš nedaugelio Lietuvos švyturių, į kurį leidžiama laisvai įlipti ir pasižvalgyti. Į švyturio apžvalgos aikštelę veda seni geležiniai, papuošti originaliais {ornamentais laiptai. Objektas svarbus ir kaip regykla, nuo kurios atsiveria Kuršių marios, matosi Kuršių nerija, Rusnės sala. Nuo šios aikštelės galima matyti Kuršių nerijoje už 12-13 km į pietvakarius esančią Nidą ir auksines kopas bei 8,5 km į vakarus nutolusią Preilą. Už 3,6 km į rytus į Kuršių marias įteka pagrindinė Nemuno atšaka Atmata.",
                   longitude: 21.1896229,
                   latitude: 55.3410012,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Šiaulės apžvalgos bokštas (21 m)",
                   description: "Apžvalgos bokštas pastatytas tarp Šiaulėnų ir Šaukoto esančiuose kalneliuose, nuo kurio atsiveria fantastiški Šiaulėnų ir Šaukoto apylinkių vaizdai. Užlipus į jį atsiveria kvapą gniaužianti panorama: galima pamatyti ne tik Radviliškio miestą ir jo apylinkes, bet ir Rėkyvą. Prie bokšto įrengtas pėsčiųjų takas, juo galima nueiti iki Kudinų piliakalnio, kitaip vadinamu Šiaulės kalnu. Netoliese yra Varpinės kalnas, šiek tiek atokiau – Kartuvių kalnas. ",
                   longitude: 23.4192118,
                   latitude: 55.6455652,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Senosios gaisrinės bokštas Ukmergėje",
                   description: "18 m. aukščio bokštas pastatytas 19 a. pabaigoje. Jo viršuje gaisrininkai stebėdavo apylinkes. Bokšte kabojo varpas, kuriuo skambindavo kilus gaisrui. Čia pat buvo arklidės ir vežimas su vandens statine. Gelbėtojų komanda šiuo vežimu skubėdavo į nelaimės vietą. Dabar šis unikalus statinys – puiki vieta apžvelgti Ukmergės miestą. Nuo jo matosi Šv. Apaštalų Petro ir Povilo bei Švč. Trejybės bažnyčios. Bokšto raktai – Ukmergės kraštotyros muziejuje.",
                   longitude: 24.7645622,
                   latitude: 55.2492522,
                   user: User.first,
                   place_type: PlaceType.first
               }, {
                   name: "Nidos apžvalgos bokštas (10 m)",
                   description: "Velniškai gražus statinys, kurio projekto autoriai yra prancūzų architektai, į pagalbą pasitelkę lietuvių jaunuosius architektus, Europos architektūros studentų asamblėją.<br><br>Įrenginys labai „išmanus“. Apatinėje dalyje yra sūpynės, vidury - laiptai, amfiteatriukas, o viršuje galima stebėti jūrą, dangų, saulėlydžius ir saulėtekius. Prancūzai, sukūrę šį projektą, nusprendė, kad viską darys tik iš natūralių medžiagų.<br><br>Dėl saugumo žiemą bokštą planuojama uždaryti.",
                   longitude: 20.9902503,
                   latitude: 55.3044164,
                   user: User.first,
                   place_type: PlaceType.first
               }])

Article.create!([
                    {title: 'Lorem Ipsum', user: User.first, body: 'Fusce condimentum pretium est eu porta. Sed vitae enim vel leo mollis suscipit at vel urna. Aenean a semper nibh. Suspendisse imperdiet metus id odio sollicitudin commodo. Pellentesque et rutrum nisi, non dignissim est. Nullam vitae fermentum nulla, et faucibus tortor. Morbi sit amet dolor non augue lacinia convallis sit amet eget neque. Cras ornare pellentesque purus, eu malesuada nisl aliquet ac. Donec dictum ipsum ut erat ultricies, et dictum urna iaculis.'},
                    {title: 'Lorem Ipsum', user: User.first, body: 'Fusce eu efficitur quam. Ut pellentesque enim mattis blandit mollis. Cras in auctor risus. Morbi dapibus urna vitae dolor elementum, quis pellentesque nulla vestibulum. Phasellus ornare laoreet nibh, et tempor lorem placerat nec. Donec placerat efficitur massa in consequat. Curabitur sapien odio, pulvinar et purus eu, ultricies pulvinar risus. Duis interdum ligula ut diam interdum faucibus. Sed non nibh ipsum.'},
                    {title: 'Lorem Ipsum', user: User.second, body: 'Donec id neque posuere tortor pellentesque viverra eu in tellus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean condimentum tortor tellus, vel bibendum nisi mollis at. Proin ultrices vestibulum interdum. Nullam rhoncus condimentum dui, et ornare nulla pulvinar sit amet. Pellentesque rutrum justo ac eleifend tempus. Aenean viverra quis magna non semper. Sed et dapibus tellus. Suspendisse potenti. Aliquam quis odio at velit interdum porttitor vel ac velit. Nulla magna tortor, condimentum in arcu ut, sollicitudin bibendum nulla. Sed eu nisl suscipit, commodo ante nec, imperdiet est. Nulla tempor hendrerit sodales. Nam tortor diam, interdum sit amet arcu a, pellentesque rhoncus ipsum. In luctus nibh a varius volutpat. Vestibulum feugiat est non tellus maximus, mollis rhoncus tellus scelerisque.'},
                    {title: 'Lorem Ipsum', user: User.first, body: 'Cras ut massa tortor. Proin non massa fringilla, pulvinar ex non, commodo nisl. Curabitur sit amet mollis orci. Maecenas neque elit, porta sed rhoncus vel, semper sit amet metus. Donec purus diam, vestibulum in finibus sit amet, interdum non odio. Etiam varius hendrerit libero a porta. Nullam sed est in urna hendrerit sodales vel et tellus. In suscipit sollicitudin congue. Maecenas condimentum euismod leo, ut semper purus hendrerit vitae. Pellentesque vel nunc egestas, euismod purus malesuada, hendrerit libero. Ut mauris sem, lacinia dictum risus vel, congue mollis lorem. Nulla lacinia consequat orci id pretium. Nunc accumsan rutrum leo, sit amet posuere felis. Vivamus elementum, lacus vel pharetra feugiat, massa felis feugiat dui, in porttitor augue tortor ac eros. Sed at ullamcorper nulla, sit amet tempor leo.'},
                    {title: 'Lorem Ipsum', user: User.second, body: 'Praesent tristique, dui quis facilisis euismod, ante sem accumsan dui, quis fringilla sem erat hendrerit lacus. Nunc metus nulla, malesuada in nisl in, finibus blandit arcu. Phasellus lobortis nisi et rutrum dignissim. Nullam viverra felis id cursus elementum. Curabitur convallis tristique ligula, vel accumsan nisi bibendum vel. Nulla luctus, arcu et ullamcorper molestie, lectus urna varius massa, in posuere lectus lacus at dui. Nullam est arcu, commodo euismod enim dictum, molestie vestibulum ante. Phasellus facilisis tincidunt ipsum vel congue.'},
                    {title: 'Lorem Ipsum', user: User.second, body: 'Pellentesque nec orci id nibh consequat efficitur vel et eros. Aliquam ac semper ex, sed rhoncus odio. Proin sit amet pretium urna. Ut posuere imperdiet consectetur. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque ultricies non ex vel bibendum. Mauris id congue elit. In euismod et neque ut finibus. Aliquam erat volutpat. Etiam viverra sapien vel congue tincidunt. Aliquam mauris elit, pretium vitae erat nec, lacinia accumsan orci. Vestibulum gravida purus tellus, et accumsan magna ullamcorper nec. Suspendisse potenti.'},
                    {title: 'Lorem Ipsum', user: User.first, body: 'Praesent mattis, est vel rutrum facilisis, leo tellus pellentesque dui, vitae tempus dui leo eu nulla. Nulla eu ipsum semper, fringilla leo id, sollicitudin lorem. Aliquam sed magna in ipsum suscipit fringilla ut ut libero. Quisque eget purus a eros ultrices vestibulum nec eu turpis. Vestibulum semper tortor lobortis lorem imperdiet suscipit. Fusce blandit dui massa. Proin sed convallis libero. Aenean fringilla metus et imperdiet congue. Cras volutpat neque et luctus posuere. Duis libero nibh, laoreet a orci vel, lacinia efficitur nisl. Morbi eget mollis urna, tincidunt cursus massa. Maecenas magna lacus, iaculis sit amet dui sit amet, consectetur hendrerit felis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur magna orci, sollicitudin nec accumsan eget, sagittis sed nunc. Proin vel diam in lectus ultrices sagittis vulputate id ipsum.'},
                    {title: 'Lorem Ipsum', user: User.first, body: 'Aliquam quis risus condimentum, blandit magna tristique, malesuada tellus. Etiam scelerisque aliquam odio non semper. Proin nec nisi nec sem malesuada iaculis vitae ac tortor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean consectetur ut lacus id iaculis. Nunc non arcu vehicula, suscipit lacus vitae, efficitur est. In vehicula eget sem vehicula finibus. Duis egestas felis non dolor pretium, at consectetur nunc ornare. Vivamus maximus maximus urna, in tincidunt nunc euismod eu. Aliquam enim mi, bibendum ut massa eu, elementum tempor nulla.'},
                    {title: 'Lorem Ipsum', user: User.second, body: 'Nunc posuere, dui nec rutrum semper, justo nisi mollis quam, vitae pulvinar lorem massa et mi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras augue lorem, rutrum a consequat ac, fringilla quis mauris. Aliquam laoreet condimentum risus, id porttitor ipsum ornare a. Quisque pharetra feugiat risus quis feugiat. Aenean hendrerit at lorem et posuere. Nam felis risus, posuere vel vulputate id, laoreet sit amet nisl. Donec ac ligula vulputate, fermentum eros ac, egestas arcu. Sed quis nisl non felis rutrum venenatis non eget tortor. Pellentesque condimentum convallis ante quis eleifend. Pellentesque finibus rhoncus ante et accumsan. Nulla vehicula metus sit amet turpis mattis feugiat.'},
                    {title: 'Lorem Ipsum', user: User.third, body: 'Proin interdum tellus ligula, in dapibus tortor imperdiet sed. Aenean condimentum in orci sed euismod. Vestibulum semper accumsan convallis. Vestibulum a libero quis lectus tempor molestie id at metus. Nunc enim massa, scelerisque vel euismod sit amet, dignissim sed ipsum. Mauris mollis ligula a ante pharetra, ac rutrum orci ullamcorper. Sed rutrum feugiat vulputate. Quisque et justo a tellus elementum fermentum. Cras iaculis velit a pharetra finibus. Integer nec facilisis augue, id mollis ipsum. Fusce magna ligula, convallis faucibus diam eget, bibendum posuere ligula. Pellentesque porttitor tristique metus sit amet tincidunt. Fusce quis magna dolor. In metus sem, maximus a mattis vel, sodales eu felis. Nulla sodales blandit magna, id rutrum turpis pharetra quis. Pellentesque vulputate lacus id ipsum tristique molestie.'}
                ])

