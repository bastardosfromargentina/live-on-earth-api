class String
  def shorten(words)
    self.split(' ').first(words).join(' ')
  end
end