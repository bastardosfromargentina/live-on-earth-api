class Variables 
  def Variables.add_item(key,value)
    @hash ||= {}
    @hash[key]=value
  end

  def Variables.const_missing(key)
    @hash[key]
  end    

  def Variables.each
    @hash.each {|key,value| yield(key,value)}
  end

  Variable.all.each do |variable|
    Variables.add_item variable.name.upcase, variable.id
  end

end