class FriendshipAction 
  def FriendshipAction.add_item(key,value)
    @hash ||= {}
    @hash[key]=value
  end

  def FriendshipAction.const_missing(key)
    @hash[key]
  end    

  def FriendshipAction.each
    @hash.each {|key,value| yield(key,value)}
  end
  FriendshipAction.add_item :NEW, 0
  FriendshipAction.add_item :SENT, 1
  FriendshipAction.add_item :GOT, 2
  FriendshipAction.add_item :ACCEPTED, 3
  FriendshipAction.add_item :DELETED, 4
end