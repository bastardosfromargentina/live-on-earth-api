class ArticleStatus
  def ArticleStatus.add_item(key,value)
    @hash ||= {}
    @hash[key]=value
  end

  def ArticleStatus.const_missing(key)
    @hash[key]
  end    

  def ArticleStatus.each
    @hash.each {|key,value| yield(key,value)}
  end
  ArticleStatus.add_item :INITIAL, 0
  ArticleStatus.add_item :CREATED, 1
end 