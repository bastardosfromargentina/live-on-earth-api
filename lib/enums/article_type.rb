class ArticleType
  def ArticleType.add_item(key,value)
    @hash ||= {}
    @hash[key]=value
  end

  def ArticleType.const_missing(key)
    @hash[key]
  end    

  def ArticleType.each
    @hash.each {|key,value| yield(key,value)}
  end
  ArticleType.add_item :SIMPLE, 0
  ArticleType.add_item :TRIP_ROUTE, 1
  ArticleType.add_item :PLACE, 2

end
