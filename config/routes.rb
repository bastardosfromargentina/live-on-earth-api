Rails.application.routes.draw do

  root to: 'users#index'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  resources :trip_routes
  resources :articles, only: [:index, :show, :create] do
    resources :comments
    resources :article_photos, only: [:index, :new, :create, :destroy]
  end
  resources :article_photos, only: [:new, :create, :destroy]
  resources :places do
    resources :place_photos, only: [:index, :new, :create, :destroy]
  end
  get 'user_places', to: 'places#user_places'
 
  get 'search_trip_routes', to: 'trip_routes#search'
  get 'search_places', to: 'places#search'
  get 'user_routes', to: 'trip_routes#user_routes'
  get 'take_trip/:id', to: 'trip_routes#take_trip', as: :take_trip  
  post 'update_trip_places', to: 'trip_routes#update_trip_places'

  get 'users_list', to: 'users#list_users', as: :people
  
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  get 'my', to: 'users#current_user_page'
  resources :users do
    resources :photos, only: [:index, :new, :create, :destroy]
    resources :articles do
      resources :comments
    end
  end

  resources :chatrooms do
    resource :chatroom_users
    resources :messages
  end
end
