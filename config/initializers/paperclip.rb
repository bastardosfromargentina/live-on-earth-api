def s3_credentials
  {
    bucket: "live-on-earth",
    access_key_id: ENV["AWS_ACCESS_KEY_ID"],
    secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"]
  }
end

Rails.application.config.before_initialize do
  Paperclip::Attachment.default_options[:url] = ':live-on-earth.s3.amazonaws.com'
  Paperclip::Attachment.default_options[:path] = '/:rails_env/:class/:attachment/:id_partition/:style/:filename'
end

Paperclip::Attachment.default_options[:s3_host_name] = 's3-us-west-2.amazonaws.com'
Paperclip::Attachment.default_options[:s3_region] = 'us-west-2'
Paperclip::Attachment.default_options[:bucket] = 'live-on-earth'