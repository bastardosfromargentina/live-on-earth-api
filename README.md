# ToDo

* fix layout

* model test

* user manual

* 

# Setup

* Ruby version 2.4.0

* Rails 5.0.2

* PostgreSQL DB

* bundle

* rails db:schema:load or rails db:setup

* rails db:seed

* rails bower:install

* bin/redis.sh - start redis

* bin/hard_db_resest.sh - recreate database
