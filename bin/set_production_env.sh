heroku rake db:schema:load DISABLE_DATABASE_ENVIRONMENT_CHECK=1  && heroku rake db:seed && heroku rake assets:clean && heroku rake assets:precompile

RAILS_ENV=production rails db:create
RAILS_ENV=production rails assets:clean
RAILS_ENV=production rails assets:precompile
RAILS_ENV=production rails db:schema:load DISABLE_DATABASE_ENVIRONMENT_CHECK=1
RAILS_ENV=production rails db:seed
rails s -e production

git push heroku master
heroku rake db:schema:load DISABLE_DATABASE_ENVIRONMENT_CHECK=1 RAILS_ENV=production
heroku rake db:seed RAILS_ENV=production
heroku rake assets:clean RAILS_ENV=production
heroku rake assets:precompile RAILS_ENV=production

